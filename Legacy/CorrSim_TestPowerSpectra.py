import math
import importlib
import numpy as np
import matplotlib.pyplot as plt
import CorrSim_FunctionsMain as corrsim
import matplotlib.lines as mlines

importlib.reload(corrsim)

## ========== Fourier Parameters ==========

## ----- Power Spectrum Parameters -----

'''
	-Power Spectral Parameters-
Here, one defines the shape of the power spectra, and the coherence of the two bands.
There are currently two types of power spectral models implemented: broken_powerlaw, and lorentzian

	- broken_powerlaw:	Define a single broken power law for both the power spectra and the coherence.
						This is the simpler of the two models.

						The broken power law for both power spectra is the same shape, and are defined by a power
							and a break frequency (their differing normalisations will come later, based on F_rms).
						For the coherence, you must define a power, break frequency and a constant.

	- lorentzians:		Define lorentzians for both data sets.
						This is the more complex model, but the fine coherence control makes it more realistic.

						Lorentzians are defined using 3 parameters (Normalisation, Midpoint and Width)
						Band 2 Lorentzians take a 4th parameter, the coherence fraction (how coherent they are with Band 1)
						In both cases, this is done via a single 1-D list which is later parsed automatically.

Note that the overall normalisation of the power spectra is based on the F_rms values for each band.
	Therefore, the white noise parameter, as well as the 'Norm' parameter in the lorentzians, will only
	adjust the relative contributions of each component, and not the overall normalisation.

'''


##	===============================================================
##	===============			Input Parameters		===============

## ----- Name -----

output_dir				= 'CorrSim_Outputs'		## Define output directory (Will be created if it doesn't exist)
fileprefix				= 'CorrSim - Test - '	## Define some prefix; this will be appended to all filenames.

## ----- Observation Parameters -----

## Set length of observation and time resolution (required to find the frequencies)
length_of_observation	= 1027		## Length of Observation in Seconds
time_resolution			= 2**-5		## Time Resolution in Seconds


## ----- Power Spectral Parameters -----

# power_model_type = "lorentzians"
power_model_type = "broken_powerlaw"

if power_model_type == "broken_powerlaw":

	## Power Spectrum parameters:
	ps_power		= -1.5			## Power Law component
	break_freq		= 1				## Break Frequency of the power law

	## Coherence parameters:
	coh_constant	= 0.1			## Constant Coherence
	coh_power		= -0.5			## Power Law component for Coherence
	coh_break_freq	= 0.5			## Break Frequency of the power law for the Coherence


elif power_model_type == "lorentzians":

	## Simplified
	# Band A Lorentzians
	Lorentz_params_A	= [
		##		Norm		Width		Midpoint
				45,		0.5,	0
			,	40,		4,		0
			,	25,		0.15,	0.1
			,	10,		0.1,	0.3
			,	3,		3,		3
	]

	## Band B Lorentzians
	Lorentz_params_B	= [
		##		Norm	Width	Midpoint	Fraction Coherent
				75,		0.1,	0,			1/200
			,	50,		0.1,	0.1,		1/5
			,	45,		2,		0,			1/150
			,	3,		10,		8,			0
	]




## ===== Calculations =====

print("Creating model Power Spectra...")

## --- Define frequencies

obs_length	= length_of_observation
time_res	= time_resolution

freq_min	= 1 / obs_length						## Minimum Frequency
freq_max	= 1 / (2*time_res)						## Maximum Frequency
freq_res	= 1 / obs_length						## Frequency Resolution

frequencies	= np.arange(0, freq_max+(freq_res*0.1), freq_res)


##		Power Spectrum Parameters
##	----------------------------------

if power_model_type == "broken_powerlaw":

	print("Using: Broken Powerlaw")

	power_model = corrsim.power_model_broken_powerlaw(frequencies, ps_power, break_freq, coh_constant, coh_power, coh_break_freq)


elif power_model_type == "lorentzians":

	print("Using: Lorentzians")

	power_model = corrsim.power_model_lorentzians(frequencies, Lorentz_params_A, Lorentz_params_B)

	Lorentzians_A				= power_model[4]
	Lorentzians_B_coh			= power_model[5]
	Lorentzians_B_inc			= power_model[6]
	num_lorentz_A				= power_model[7]
	num_lorentz_B				= power_model[8]


power_spectrum_A		= power_model[0]
power_spectrum_B_coh	= power_model[1]
power_spectrum_B_inc	= power_model[2]

power_spectrum_B		= power_spectrum_B_coh + power_spectrum_B_inc





###
### ===== Plotting =====
###

### ==== Plot Model Power Spectra ===

print("Plotting...")


fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(7,7))

## --- 1: Power
ax = axs[0]
ax.set_xscale('log')

ax.plot(frequencies[1:len(frequencies)],  power_spectrum_A[1:len(frequencies)], linestyle="-", label="Band A", color="blue")
ax.plot(frequencies[1:len(frequencies)],  power_spectrum_B[1:len(frequencies)], linestyle="-", label="Band B", color="red")

ax.legend(loc="lower center")

y_min = min([min((power_spectrum_B)[1:len(frequencies)]), min((power_spectrum_A)[1:len(frequencies)])])
y_max = max([max((power_spectrum_B)[1:len(frequencies)]), max((power_spectrum_A)[1:len(frequencies)])])

ax.set_xlim(frequencies[1]/2, max(frequencies)*2)
ax.set_ylim(y_min/2, y_max*2)

ax.set_xscale("log")
ax.set_yscale("log")

ax.set_xlabel('Frequency (Hz)')
ax.set_ylabel('Frac. RMS Power')

ax.grid(linestyle=":", zorder=5)


## --- 2: Frequency * Power
ax = axs[1]
ax.set_xscale('log')

ax.plot(frequencies[1:len(frequencies)],  frequencies[1:len(frequencies)]*power_spectrum_A[1:len(frequencies)], linestyle="-", label="Band A", color="blue")
ax.plot(frequencies[1:len(frequencies)],  frequencies[1:len(frequencies)]*power_spectrum_B[1:len(frequencies)], linestyle="-", label="Band B", color="red")

ax.legend(loc="lower center")

y_min = min([min((frequencies*power_spectrum_B)[1:len(frequencies)]), min((frequencies*power_spectrum_A)[1:len(frequencies)])])
y_max = max([max((frequencies*power_spectrum_B)[1:len(frequencies)]), max((frequencies*power_spectrum_A)[1:len(frequencies)])])

ax.set_xlim(frequencies[1]/2, max(frequencies)*2)
ax.set_ylim(y_min/2, y_max*2)

ax.set_xscale("log")
ax.set_yscale("log")

ax.set_xlabel('Frequency (Hz)')
ax.set_ylabel('Frequency * Frac. RMS Power')

ax.grid(linestyle=":", zorder=5)

plt.subplots_adjust(bottom=0.1, left=0.15, top=0.98, right=0.98, hspace=0)

fileOut = "./" + output_dir + "/" + fileprefix + "Model Power Spectra.png"
plt.savefig(fileOut, bbox_inches='tight')

plt.close('all')



###
### ===== Plot Power Spectra Components =====
###


fig, axs = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=True, figsize=(10,7))

## --- 1a: Power A
ax = axs[0,0]

ax.set_xlabel('Frequency (Hz)')
ax.set_ylabel('Frac. RMS Power')

if power_model_type == "broken_powerlaw":

	ax.plot(frequencies[1:len(frequencies)], power_spectrum_A[1:len(frequencies)], linestyle = '--', color="blue", alpha=0.5)

elif power_model_type == "lorentzians":

	for i in range(num_lorentz_A):
		ax.plot(frequencies[1:len(frequencies)], Lorentzians_A[1:len(frequencies),i], linestyle = '--', color="blue", alpha=0.5)

ax.plot(frequencies[1:len(frequencies)], power_spectrum_A[1:len(frequencies)], linewidth = 3, linestyle = '-', color="blue")

line1 = mlines.Line2D([], [], color='blue',		linestyle='-',	label='A (Overall)')
line2 = mlines.Line2D([], [], color='blue',		linestyle='--',	label='A (Components)')

if power_model_type == "broken_powerlaw":
	ax.legend(handles=[line1], loc="lower center")
elif power_model_type == "lorentzians":
	ax.legend(handles=[line1, line2], loc="lower center")

ax.set_xscale("log")
ax.set_yscale("log")

ax.grid(linestyle=":", zorder=5, linewidth=0.5)


## --- 1b: Power B
ax = axs[0,1]

ax.set_xlabel('Frequency (Hz)')

if power_model_type == "broken_powerlaw":
	ax.plot(frequencies[1:len(frequencies)], power_spectrum_B_coh[1:len(frequencies)], linestyle = '-.', alpha=0.5, color="purple")
	ax.plot(frequencies[1:len(frequencies)], power_spectrum_B_inc[1:len(frequencies)], linestyle = ':',  alpha=0.5, color="red")

elif power_model_type == "lorentzians":
	for i in range(num_lorentz_B):
		ax.plot(frequencies[1:len(frequencies)], Lorentzians_B_coh[1:len(frequencies),i],linestyle = '-.', alpha=0.5, color="purple")
		ax.plot(frequencies[1:len(frequencies)], Lorentzians_B_inc[1:len(frequencies),i],linestyle = ':',  alpha=0.5, color="red")

ax.plot(frequencies[1:len(frequencies)], power_spectrum_B[1:len(frequencies)], linewidth = 3, linestyle = '-', color="red")

line4 = mlines.Line2D([], [], color='red',		linestyle='-',	label='B (Overall)')
line5 = mlines.Line2D([], [], color='purple',	linestyle='-.',	label='B (Coherent)')
line6 = mlines.Line2D([], [], color='red',		linestyle=':',	label='B (Incoherent)')

ax.legend(handles=[line4, line5, line6], loc="lower center")

ax.set_xscale("log")
ax.set_yscale("log")

ax.grid(linestyle=":", zorder=5, linewidth=0.5)



## --- 2a: Frequency * Power A
ax = axs[1,0]

ax.set_xlabel('Frequency (Hz)')
ax.set_ylabel('Frequency * Frac. RMS Power')

if power_model_type == "broken_powerlaw":

	ax.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*power_spectrum_A[1:len(frequencies)], linestyle = '--', color="blue", alpha=0.5)

elif power_model_type == "lorentzians":

	for i in range(num_lorentz_A):
		ax.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*Lorentzians_A[1:len(frequencies),i], linestyle = '--', color="blue", alpha=0.5)

ax.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*power_spectrum_A[1:len(frequencies)], linewidth = 3, linestyle = '-', color="blue")

line1 = mlines.Line2D([], [], color='blue',		linestyle='-',	label='A (Overall)')
line2 = mlines.Line2D([], [], color='blue',		linestyle='--',	label='A (Components)')

if power_model_type == "broken_powerlaw":
	ax.legend(handles=[line1], loc="lower center")
elif power_model_type == "lorentzians":
	ax.legend(handles=[line1, line2], loc="lower center")

ax.set_xscale("log")
ax.set_yscale("log")

ax.grid(linestyle=":", zorder=5, linewidth=0.5)


## --- 2b: Frequency * Power B
ax = axs[1,1]

ax.set_xlabel('Frequency (Hz)')

if power_model_type == "broken_powerlaw":

	ax.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*power_spectrum_B_coh[1:len(frequencies)], linestyle = '-.', alpha=0.5, color="purple")
	ax.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*power_spectrum_B_inc[1:len(frequencies)], linestyle = ':',  alpha=0.5, color="red")

elif power_model_type == "lorentzians":

	for i in range(num_lorentz_B):
		ax.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*Lorentzians_B_coh[1:len(frequencies),i],linestyle = '-.', alpha=0.5, color="purple")
		ax.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*Lorentzians_B_inc[1:len(frequencies),i],linestyle = ':',  alpha=0.5, color="red")

ax.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*power_spectrum_B[1:len(frequencies)], linewidth = 3, linestyle = '-', color="red")

line4 = mlines.Line2D([], [], color='red',		linestyle='-',	label='B (Overall)')
line5 = mlines.Line2D([], [], color='purple',	linestyle='-.',	label='B (Coherent)')
line6 = mlines.Line2D([], [], color='red',		linestyle=':',	label='B (Incoherent)')

ax.legend(handles=[line4, line5, line6], loc="lower center")

ax.set_xscale("log")
ax.set_yscale("log")

ax.grid(linestyle=":", zorder=5, linewidth=0.5)



plt.subplots_adjust(bottom=0.1, left=0.15, top=0.98, right=0.98, hspace=0, wspace=0)

fileOut = "./" + output_dir + "/" + fileprefix + "Power Spectrum Components.png"
plt.savefig(fileOut, bbox_inches='tight')

plt.close('all')





# ### ===== Plot Model Lags =====
#
# print("Plotting...")
#
# fig, axs = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(6,6))
#
# ## --- Phase Lags ---
# ax = axs[0]
# ax.set_xscale('log')
#
# ax.yaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
# ax.yaxis.set_minor_locator(plt.MultipleLocator(np.pi / 4))
# ax.yaxis.set_major_formatter(plt.FuncFormatter(corrsim.format_func))
# ax.set_ylim([-3.8, 3.8])
#
# ax.plot(frequencies, model_phase_lags-(4*np.pi),	lw=2, zorder=3, linestyle=':',  color="red", label = "-4π")
# ax.plot(frequencies, model_phase_lags-(2*np.pi),	lw=2, zorder=3, linestyle='--', color="firebrick", label = "-2π")
# ax.plot(frequencies, model_phase_lags, 				lw=2, zorder=3, linestyle='-',  color="black", label = "±0")
# ax.plot(frequencies, model_phase_lags+(2*np.pi),	lw=2, zorder=3, linestyle='--', color="mediumblue", label = "+2π")
# ax.plot(frequencies, model_phase_lags+(4*np.pi),	lw=2, zorder=3, linestyle=':',  color="dodgerblue", label = "+4π")
#
# ax.axhline(+np.pi, linestyle="-", linewidth=1.5, color="darkgrey", zorder=2)
# ax.axhline(-np.pi, linestyle="-", linewidth=1.5, color="darkgrey", zorder=2)
#
# ax.legend()
#
# ax.set_ylabel("Phase Lag (Radians)")
# ax.grid(linestyle='--')
#
# if time_or_phase == "phase":
# 	for spine in ['top', 'right', 'bottom', 'left']:
# 		ax.spines[spine].set_linewidth(2)
# 		ax.spines[spine].set_color("red")
# 		ax.spines[spine].set_zorder(10)
# else:
# 	ax.spines["bottom"].set_linewidth(2)
# 	ax.spines["bottom"].set_color("red")
# 	ax.spines["bottom"].set_zorder(10)
#
#
# ## --- Time Lags ---
# ax = axs[1]
# ax.set_xscale('log')
# ax.set_yscale('log', nonposy="clip")
#
# ax.plot(frequencies, model_time_lags,	linestyle="-",  lw=2, color="firebrick", label = "+ve Lags")
# ax.plot(frequencies, -model_time_lags,	linestyle="--", lw=2, color="blue", label = "-ve Lags")
#
# ax.legend()
#
# ax.set_ylabel("Time Lag (s)")
# ax.set_xlabel("Frequency (Hz)")
# ax.grid(linestyle='--')
#
# plt.subplots_adjust(bottom=0.1, left=0.15, top=0.98, right=0.98, hspace=0)
#
# if time_or_phase == "time":
# 	for spine in ['top', 'right', 'bottom', 'left']:
# 		ax.spines[spine].set_linewidth(2)
# 		ax.spines[spine].set_color("red")
# 		ax.spines[spine].set_zorder(10)
# else:
# 	ax.spines["top"].set_linewidth(2)
# 	ax.spines["top"].set_color("red")
# 	ax.spines["top"].set_zorder(10)
#
# ## --- Export ---
#
# fileOut = "./" + output_dir + "/" + fileprefix + "Test Lags.png"
# plt.savefig(fileOut, bbox_inches='tight')
#
# plt.close('all')

print("Plotted model Power Spectra.")
