###  CorrSim - A Correlated Timing Observation Simulator  ###

>                      By John A Paice                      <
> With invaluable help from Poshak Gandhi and Ranjeev Misra <

Thank you for downloading CorrSim! I hope you find it useful in your projects.

The main aim of this code is to:
1: Help in planning multiwavelength timing observations, and
2: Aid in the interpretation of these observations.

If this project was indeed useful to you in your research, please include the following acknowledgement:
“This work made use of the \texttt{CorrSim} software.”
With a footnote linking to the appropriate gitlab page.

To cite the work, please cite Paice et al. (in Prep)


## CorrSim’s function:
CorrSim takes a variety of input parameters, and with them, creates two lightcurves - Series A and Series B. These lightcurves are related, as defined by given coherence and time lag parameters. This is meant to simulate a multiwavelength observation.

CorrSim’s main role compared to other simulation software is that it gives you fine control over how these lightcurves are related to each other, in a Fourier and Timing sense.

Firstly, it allows you to define the power spectra of the lightcurves, either through a broken power law or a series of Lorentzians. Through this, you can also set how coherent the lightcurves are with each other - i.e. how much they are correlated with each other as a function of Fourier frequency.

Secondly, it allows you to define the phase lags or time lags between the lightcurves, with respect to Fourier frequency. These are defined by essentially ‘plotting’ the lags on a graph.


## How to use:

Currently, the input parameters are modified by editing the the CorrSim_Main.py file. The first part of the file has a variety of input parameters that can be edited. They are described at the end of this file in ‘Glossary: Input Parameters’.

The Power Spectra and Phase/Time lags are defined in a similar way. To help you create them, two additional files are included; CorrSim_TestPowerSpectra.py and CorrSim_TestLags.py. These will just plot your inputs, so you can check what you’re telling the program to create.

By running the program, it will automatically create text files containing:
- A log file containing your parameters
- Your model Fourier plots (Power Spectra, Coherence, Phase Lags, Time Lags)
- The model Cross-Correlation Function (CCF)
- The simulated power spectra (F_rms Normalised)
- The simulated lightcurves

Optionally, the code can also give you the following:
- Plot of the Model CCF
- Plot of the Model Power Spectrum
- Plot of the Model Power Spectrum Components
- Plot of the Model Lags
- Plot of the Lightcurves
- Plot of the power spectrum made from the lightcurves
- Plot of a power spectra made from N simulated lightcurves
- Text file of the CCF made from the lightcurves (averaged over a given number of segments)
- Plot of the CCF made from the lightcurves
- Plot of the output Fourier data (Power Spectra, Coherence, and Phase/Time lags) found from the simulated lightcurves.
- Plot of the output Fourier data with the models overplotted
- A large compilation plot of the inputs and outputs.


## Warnings about use:

- The code automatically created text files containing the Fourier models and lightcurves. For large numbers of bins (>1e6), these can be very large (>100 MB). Be aware of the size of the files you are creating!

- Particularly long or high-resolution observations can be taxing on your system, and may make CorrSim take a long time to run. A warning is built in that requires confirmation if your number of bins goes over 1e6. You can also optimise the number of bins using optimise_bins = 2 (adjusts to a power of 2) or 7 (adjusts to a ‘7-smooth’ number, i.e. a number with no factor higher than 7).


## Glossary of Parameters:

— Source & Observation Parameters: —

output_dir (String)
Defines the output directory; e.g. ‘CorrSim_Outputs’ will save all outputs to the folder ./CorrSim_Outputs. This folder will be created if it doesn't already exist.

fileprefix (String)
Defines a prefix for the filenames of all outputted files.

length_of_observation (Number, >0)
Length of simulated observation (seconds). Particularly relevant for the number of bins, which is the biggest factor in the size of the output files and the speed of CorrSim.

time_resolution (Number, >0)
Time resolution of simulated observation (seconds). Particularly relevant for the number of bins, which is the biggest factor in the size of the output files and the speed of CorrSim.

optimise_bins (2/7/n)
Optional; adjusts the length of the observation.
If 2, the number of bins is changed to the nearest power of 2. This makes the code run the fastest, and is good for Fourier calculations.
If 7, the number of bins is changed to a 7-smooth number (i.e. a number with no factors greater than 7). This makes the code run relatively fast, with more choice than selecting a power of 2.
If not desired, use any other number or string.

mean_counts_per_sec_A (Number, >0)
Mean count rate in Series A (Counts/s)

mean_counts_per_sec_B (Number, >0)
Mean count rate in Series B (Counts/s)

F_rms_A (Number)
Fractional RMS in Series A.
For example: for the X-ray lightcurve of X-ray Binaries, F_rms = ~0.3 in the hard state, and ~0.04 in the Soft State.

F_rms_B (Number)
Fractional RMS in Series B.
For example: for the Optical lightcurve of X-ray Binaries, F_rms = ~0.1.


— Noise Parameters: —

apply_red (A/B/AB/n)
Applies ‘red noise’ to either Series A, B, both, or neither respectively.
Red noise is also known as ‘Brownian noise’, and is proportional to frequency^-2.
CorrSim applies red noise to the power spectra before they’re converted to lightcurves, using the methods of Timmer & Koenig 1995 (1995A&A...300..707T):
For each Fourier Frequency, two Gaussian distributed random numbers are drawn.
These are multiplied by sqrt(0.5*(power_spectrum)).
The result is the real and imaginary part of the Fourier transformed data.

apply_poisson (A/B/AB/n)
Applies ‘poisson noise’ to either Series A, B, both, or neither respectively.
Poisson noise, also known as ‘shot noise’, comes from the random nature of emitted photons.
CorrSim applies poisson noise to the lightcurves by drawing a random number from a poisson distribution for each bin, with the mean centred on the flux in that bin.

apply_scintillation (A/B/AB/n)
Applies ‘scintillation noise’ to either Series A, B, both, or neither respectively.
This noise comes from atmospheric effects, and is thus only applicable to simulations from ground-based observatories.
Scintillation noise is dependant on a telescopes diameter, altitude, and exposure time, a target’s altitude, the height of atmospheric turbulence, and some empirical value. Default values are for the NTT at La Silla, Chile, with values taken from Osborn et al. 2015 (DOI: 10.1093/mnras/stv1400).

apply_readout (A/B/AB/n)
Applies ‘readout noise’ to either Series A, B, both, or neither respectively.
Readout noise comes from fluctuations in reading out charge from a CCD.
This is dependant on ‘read_noise_A’ and ‘read_noise_A’, defined below.

remove_whitenoise (y/n)
This is used for plotting the recovered power spectra and coherence.
Note that this is a VERY simple approximation - it assumes that the last point in the power spectrum is white-noise dominated and removes 99% of that value from the power spectra.
This is ONLY a good approximation when the spectrum is white-noise dominated at the highest Fourier frequency.

empirical_value_A (Number, >0)
An empirical coefficient that varies depending on the site of the telescope. This is defined as C_Y in Osborn et al. 2015 (DOI: 10.1093/mnras/stv1400), where several sites are listed. The mean value is 1.5.
An increase in this value increases the scintillation noise.

telescope_diameter_A (Number, >0)
Diameter of the observing telescope (metres).
An increase in this value decreases the scintillation noise.

telescope_altitude_A (Number, >0)
Altitude of the observing telescope (metres)
An increase in this value decreases the scintillation noise.

exposure_time_A (Number, < time_resolution)
Exposure time of the observing telescope. This is different from time resolution, as it is does NOT include the time to read out information from the CCD. 
An increase in this value decreases the scintillation noise.

target_altitude_A (Number, 0-90)
Altitude of the source (degrees).
An increase in this value decreases the scintillation noise.

turbulence_height_A (Number, >0)
Height of turbulence in the atmosphere (metres). A typical value here is around 8000.
An increase in this value increases the scintillation noise.

empirical_value_B (Number, >0)
An empirical coefficient that varies depending on the site of the telescope. This is defined as C_Y in Osborn et al. 2015 (DOI: 10.1093/mnras/stv1400), where several sites are listed. The mean value is 1.5.
An increase in this value increases the scintillation noise.

telescope_diameter_B (Number, >0)
Diameter of the observing telescope (metres).
An increase in this value decreases the scintillation noise.

telescope_altitude_B (Number, >0)
Altitude of the observing telescope (metres)
An increase in this value decreases the scintillation noise.

exposure_time_B (Number, < time_resolution)
Exposure time of the observing telescope. This is different from time resolution, as it is does NOT include the time to read out information from the CCD. 
An increase in this value decreases the scintillation noise.

target_altitude_B (Number, 0-90)
Altitude of the source (degrees).
An increase in this value decreases the scintillation noise.

turbulence_height_B (Number, >0)
Height of turbulence in the atmosphere (metres). A typical value here is around 8000.
An increase in this value increases the scintillation noise.

read_noise_A (Number, >0)
Readout noise from a CCD in electrons. Applicable if apply_readout = A or AB.

read_noise_B (Number, >0)
Readout noise from a CCD in electrons. Applicable if apply_readout = B or AB.


— Plotting Parameters: —

plot_model_ccf (y/n)
Plots the model Cross-Correlation Function (A time-domain representation of how the two lightcurves correlate as a function of time lag. 1 = perfect correlation, 0 = no correlation, -1 = perfect anti-correlation).

plot_model_ps (y/n)	
Plots the model power spectra (A measure of the ‘power’ at each Fourier Frequency; i.e. how much the lightcurve varies at that frequency).

plot_model_ps_comps (y/n)
As plot_model_ps, but also with the individual components plotted.

plot_model_lags (y/n)
Plots the model lags (A measure of how much Series B lags Series A as a function of Fourier frequency).

plot_lightcurves	 (y/n)
Plots both lightcurves (Series A = Blue, Series B = Red), with zoomed-in sections.

calculate_recovered_ps (y/n)
Calculates and plots power spectra calculated from the simulated lightcurves. These will be affected by noise and the finite observation, and will thus be different than the model power spectra inputted.

average_ps_iterations (Number)
If >1, the code simulates that many lightcurves and produces a power spectrum for each one. An average power spectrum is then calculated and plotted. This is still affected by noise, but mitigates the problem of finite sampling.

calculate_recovered_ccf (y/n)
Calculates and plots the Cross-Correlation Function (CCF) of the simulated lightcurves. This CCF is made by splitting up the lightcurves into segments (defined by recovered_ccf_segment), running CCF analysis on each of them, and then averaging the result.

recovered_ccf_segment (Number)
Segment size of averaged Cross Correlation Function described in calculate_recovered_ccf (seconds).

recovered_ccf_binning (Integer)
Optional; bins the data before running the Cross Correlation Function described in calculate_recovered_ccf. The number given is the number of bins it averages over (0 or 1 = no binning).

calculate_fourier (y/n)
Calculates and plots the power spectra, coherence, and phase/time lags between the two series. It accomplishes this by splitting up the lightcurves in a number of segments (size given by fourier_bins), carrying out the analysis on each segment, and then averaging the result. These will be different from the inputted values, due to the effects of noise and finite sampling.

fourier_bins (Integer)
Number of bins per segment in Fourier analysis (ideally, this should be a power of 2)

fourier_rebin (Number)
Amount of logarithmic rebinning to do when calculating and plotting the Fourier analysis.

reference_freq (Number)
This is used when calculating the phase lags. Phase lags are inherently constrained between +/-pi. If the actual phase lags are outside of this range, e.g. between pi and 2pi, then analysis will show that they are between -pi and 0 radians due to the cyclical nature of sine waves. A ‘reference frequency’ is thus defined to be the frequency at which the code calibrates the rest of the phase lags; this should be the frequency at which the measured phase lag is between +/-pi.

plot_fourier_models (y/n)
Similar to calculate_fourier, but also plots the models on the same plots.

plot_compilation (y/n)
Plots a compilation of the lightcurves, model CCF, recovered CCF, Fourier inputs, and Fourier outputs (Requires calculate_fourier and calculate_recovered_ccf).


— Power Spectral Parameters: —
The power spectra can be defined either through a broken powerlaw model or a Lorentzian model.
CorrSim_TestPowerSpectra.py can be used to experiment and test inputs until it produces what you like!

power_model_type (broken_powerlaw/lorentzians)
Sets the type of power spectra to be used.
‘broken_powerlaw’ uses a simple model of constant power that breaks at some frequency, and then becomes a power law component that decreases with increasing frequency. The coherence is described in the same way. 
‘lorentzians’ allows you to define a series of Lorentzians that sum to the power spectrum for each series. Series B includes an extra parameter for each Lorentzian to define how coherent it is with Series A.


Parameters for the Broken Powerlaw model:

ps_power (Number)
Index of the Power Law component of the power spectra.

break_freq (Number, >0)
Break Frequency of the power law (the frequency at which it transitions from a constant to a power law)

coh_constant (Number, >0)
Index of the Power Law component of the coherence.

coh_power (Number)
Break Frequency of the coherence (the frequency at which it transitions from a constant to a power law)

coh_break_freq (Number, >0)
Break Frequency of the coherence (the frequency at which it transitions from a constant to a power law)


Parameters for the Lorentzian model:

Lorentz_params_A (List, length divisible by three)
A series of parameters defining Lorentzians. Each group of three in the list is the normalisation, width, and midpoint of a Lorentzian.

Lorentz_params_B (List, length divisible by four)
As Lorentz_params_A, but must be divisible by four; the fourth parameter in each group is how coherent that Lorentzian is with Series A (between 0 and 1).


— Lag Parameters —
Lags can be defined either as time lags or phase lags. Time lags are defined in log-log space, while phase lags are defined in semi-log space.
Lags are defined by defining several sections of different distributions. Think of it as drawing the lag plot; define the distribution you want (e.g. constant, linear), the starting frequency, the ending frequency, and then any required parameters. The programs themselves contain hefty explanations of the method and the distributions, as well as an example.
CorrSim_TestLags.py can be used to experiment and test inputs until it produces what you like!

time_or_phase (time/phase)
Choose whether to define the time or phase lags. Whichever you don’t pick will be calculated by CorrSim from the other.

overall_lag (Number)
The time/phase lag to use for all frequencies not included in model_lag_array




## Acknowledgements

CorrSim uses the Stingray software package for some of its Fourier analysis, which can be found at https://github.com/StingraySoftware/stingray and is described in Huppenkothen et al. 2019 (DOI: 10.3847/1538-4357/ab258d). John A Paice thanks A Stevens and D Huppenkothen for help with the software. John A Paice also thanks D. Ashton for spectral timing help.
Some observational parameters given as examples were taken by GTC/HiPERCAM and NICER; we thank the teams of both of those telescopes, particularly our HiPERCAM observers Vik Dhillon and Stuart Littlefair. Full acknowledgements and analysis are described in Paice et al. 2019 (DOI: 10.1093/mnrasl/slz148).