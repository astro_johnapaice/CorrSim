## Main code for CorrSim. Use alongside CorrSim_Functions.

import os
import importlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as patches
import CorrSim_Functions as corrsim
import pandas as pd
import scipy
import sys

importlib.reload(corrsim)


##	===============================================================
##	===============			Input Parameters		===============

## ========== Source & Observation Parameters ==========

## ----- Name -----

output_dir				= 'CorrSim_Outputs'		## Define output directory (Will be created if it doesn't exist)
fileprefix				= 'CorrSim - TestA - '	## Define some prefix; this will be appended to all filenames.

## ----- Observation Parameters -----

length_of_observation	= 1000		## Length of Observation in Seconds
time_resolution			= 2**-5		## Time Resolution in Seconds

## Optional: Alter observation length to speed up calculations.
## Either choose 2 (adjusts to either a power of two; fastest, good for Fourier analysis)
## or 7 (adjusts to a 7-smooth number; fast, with more choice than powers of two).
optimize_bins			= 2			## 2/7/n
## If you don't choose 2 or 7, make sure your number of bins (length_of_observation / time_resolution)
##   does not have a large prime factor (>4 digits)

## ----- Source Parameters -----

mean_counts_per_sec_A	= 1000		## Mean Count Rate in A (Counts/s)
mean_counts_per_sec_B	= 5000		## Mean Count Rate in B (Counts/s)
F_rms_A					= 0.3		## Fractional RMS in A (For XRBs in X-rays: Hard state ~0.3, Soft State ~0.04)
F_rms_B					= 0.1		## Fractional RMS in B (For XRBs in Optical: Hard state ~0.1)

## ----- Noise Parameters -----

apply_red				= 'AB'		## A/B/AB/n - Red noise.
apply_poisson			= 'AB'		## A/B/AB/n - Poisson noise.
apply_scintillation		= 'B'		## A/B/AB/n - Scintillation noise for Ground-Based telescopes (Set Parameters Below)
apply_readout			= 'B'		## A/B/AB/n - Readout Noise (Set Parameters Below)

remove_whitenoise		= 'n'		## y/n - Assumes last point in PS is WN-dominated, and minuses that value from entire spectrum.
									## Note: VERY simple approximation. Do not use is spectrum is not WN-dominated.

## - Scintillation Noise Parameters
empirical_value_A		= 1.5						## Empirical Value from each telescope (Present: Mean)
telescope_diameter_A	= 3.58						## Telescope Diameter (m) (Present: NTT, La Silla)
telescope_altitude_A	= 2375						## Altitude of Observatory - (Present: La Silla)
exposure_time_A			= time_resolution - 0.0015	## Exposure time (s)
target_altitude_A		= 40						## Altitude of Source  (degrees)
turbulence_height_A		= 8000						## Scale height of Atmospheric Turbulence

empirical_value_B		= 1.5						## Empirical Value from each telescope (Present: Mean)
telescope_diameter_B	= 3.58						## Telescope Diameter (m) (Present: NTT, La Silla)
telescope_altitude_B	= 2375						## Altitude of Observatory - (Present: La Silla)
exposure_time_B			= time_resolution - 0.0015	## Exposure time (s)
target_altitude_B		= 40						## Altitude of Source  (degrees)
turbulence_height_B		= 8000						## Scale height of Atmospheric Turbulence

## - Readout Noise Parameters
read_noise_A			= 4.5					## Readout Noise in Electrons - HiPERCAM
read_noise_B			= 4.5					## Readout Noise in Electrons - HiPERCAM



## ========== Plotting & Calculation Parameters ==========

# ----- General Plotting Parameters -----

plot_model_ccf			= "n"		## y/n - Plots the model Cross-Correlation Function
plot_model_ps			= "y"		## y/n - Plots the model power spectra
plot_model_ps_comps		= "n"		## y/n - Plots the components of the power spectra
plot_model_lags			= "n"		## y/n - Plots the model lags used
plot_lightcurves		= "y"		## y/n - Plots both lightcurves, with zoomed-in sections

## - Recover Power Spectra
calculate_recovered_ps	= "n"		## y/n - Consistency check - converts the produced lightcurves back into Power Spectra.
average_ps_iterations	= 1 		## If >1, calculate and plot averaged Band A power spectra made from many lightcurves.

## - CCF Calculations
calculate_recovered_ccf	= "y"		## y/n - Calculates and plots the Cross-Correlation Function
recovered_ccf_segment	= 30 		## Segment size of averaged CCF, in seconds.
recovered_ccf_binning	= 0			## Optional: bins the data (0 or 1 = no binning)

## - Fourier Calculations
calculate_fourier		= "y"		## y/n - Calculates and plots the PS, coherence, and phase/time lags between the two series
fourier_bins			= 2**9		## Number of bins per segment in Fourier analysis (Ideally a power of 2)
fourier_rebin			= 0.3		## Amount of logarithmic rebinning
reference_freq			= 1			## Frequency at which the phase lag can be assumed to be correct (Not shifted +/-2pi)
plot_fourier_models		= "n"		## y/n - Plots both the models and the Fourier data on the same plot.
plot_compilation		= "y"		## y/n - Plots a compilation of the above (Requires fourier and recovered_ccf)



## ========== Fourier Parameters ==========

## ----- Power Spectrum Parameters -----

'''
	-Power Spectral Parameters-
Here, one defines the shape of the power spectra, and the coherence of the two bands.
There are currently two types of power spectral models implemented: broken_powerlaw, and lorentzian

	- broken_powerlaw:	Define a single broken power law for both the power spectra and the coherence.
						This is the simpler of the two models.

						The broken power law for both power spectra is the same shape, and are defined by a power
							and a break frequency (their differing normalisations will come later, based on F_rms).
						For the coherence, you must define a power, break frequency and a constant.

	- lorentzians:		Define lorentzians for both data sets.
						This is the more complex model, but the fine coherence control makes it more realistic.

						Lorentzians are defined using 3 parameters (Normalisation, Midpoint and Width)
						Band 2 Lorentzians take a 4th parameter, the coherence fraction (how coherent they are with Band 1)
						In both cases, this is done via a single 1-D list which is later parsed automatically.

Note that the overall normalisation of the power spectra is based on the F_rms values for each band.
	Therefore, the white noise parameter, as well as the 'Norm' parameter in the lorentzians, will only
	adjust the relative contributions of each component, and not the overall normalisation.

'''

# power_model_type = "broken_powerlaw"
power_model_type = "lorentzians"

if power_model_type == "broken_powerlaw":

	## Power Spectrum parameters:
	ps_power		= -1.5			## Power Law component
	break_freq		= 1				## Break Frequency of the power law

	## Coherence parameters:
	coh_constant	= 0.1			## Constant Coherence
	coh_power		= -0.5			## Power Law component for Coherence
	coh_break_freq	= 0.5			## Break Frequency of the power law for the Coherence


elif power_model_type == "lorentzians":

	## Simplified
	# Band A Lorentzians
	Lorentz_params_A	= [
		##		Norm	Width	Midpoint
				45,		0.5,	0
			,	40,		4,		0
			,	25,		0.15,	0.1
			,	10,		0.1,	0.3
			,	3,		3,		3
	]

	## Band B Lorentzians
	Lorentz_params_B	= [
		##		Norm	Width	Midpoint	Fraction Coherent
				75,		0.1,	0,			1/200
			,	50,		0.1,	0.1,		1/5
			,	45,		2,		0,			1/150
			,	6,		4,		3,			0
			,	3,		10,		8,			0
			,	1,		20,		25,			0
	]



## ----- Lag Parameters -----

'''
	-Lag Parameters-
Here, one defines the lags.

--- Step 1: ---
Choose whether to define your lags using time or phase lags; time_or_phase = "time"/"phase"

--- Step 2: ---
Define the overall lag. This defines what the lag should be in any undefined sections.

--- Step 3: ---
Set the lags dependant upon frequency, using either phase or time space.
This is done by 'drawing' the 'shape' of the lags using a variety of distributions.
Phase lags are described in semi-log space. Time lags are described in log-log space.

The following functions are used:
corrsim.lag_section_phase(distribution, start_freq, close_freq, start_lag, close_lag=0, \
	extra_freq=0, extra_lag=0)
corrsim.lag_section_time(distribution, start_freq, close_freq, start_lag, close_lag=0, \
	extra_freq=0, extra_lag=0)

	-Inputs:-

To define a lag, call corrsim.lag_section_time or corrsim.lag_section_time,
and define the distribution and the required parameters.

NOTE: Make sure to define your sections IN ORDER OF INCREASING FREQUENCY.
If you don't, it will create incorrect lags!


The following lag distributions are available:
	- Constant			= Constant lag, defined by start_lag.
							Requires: start_freq, close_freq, start_lag

	- Linear			= When time_or_phase="Phase": Linear in semi-log space.
							When time_or_phase="Time": Linear in log-log space.
							Requires: start_freq, close_freq, start_lag, close_lag

	- Power				= Exponentially increasing lag (Linear in normal space).
							Requires: start_freq, close_freq, start_lag, close_lag

	- Polynomial		= When time_or_phase="Phase": Polynomial in semi-log space.
							When time_or_phase="Time": Polynomial in log-log space.
							Works by solving a second-order polynomial for the three coordinates given.
							Requires: start_freq, close_freq, start_lag, close_lag, extra_freq, extra_lag

	- Constant_Time		= ! Note: Only for corrsim.lag_section_phase !
							Creates phase lags that give a constant time (set by Start Lag)
							Requires: start_freq, close_freq, start_lag

	- Constant_Phase	= ! Note: Only for corrsim.lag_section_phase_time !
							Creates time lags that give a constant phase (set by Start Lag)
							Requires: start_freq, close_freq, start_lag


The output is seven numbers for each section. These are:
	- start		= Starting frequency of the section
	- close		= Ending frequency of the section
	- A			= Constant (see below)
	- B			= Constant (see below)
	- C			= Constant (see below)
	- P			= Constant (see below)
	- log		= Boolean; defines whether or not distributions have to be calculated using logs (1=Yes, 0=No)

	A, B, C, P are constants in the expression (Ax^2 + Bx^P + C)

'''

##	- Step 1: Choose whether to define time or phase lags.
time_or_phase	= "phase"		## "time"/"phase"

model_lag_array = np.zeros((100, 7))	## Sets up the array. Don't change! If you're needing more than 100 lines, use a simpler model...
if time_or_phase == "phase":
	##	- Step 2: Set the overall lag.
	overall_lag = -4*np.pi/3			## Choose the overall (constant) lag for all non-defined sections

	##	- Step 3: Set the lag model using corrsim.lag_section_phase
	##												distribution	start_freq,	close_freq,	start_lag,	close_lag=0
	model_lag_array[0,] = corrsim.lag_section_phase("Constant"	,	0.001,  	0.02, 		-4*np.pi/3, -4*np.pi/3)
	model_lag_array[1,] = corrsim.lag_section_phase("Power"		,	 0.02,  	0.25, 		-4*np.pi/3,  2*np.pi/5)
	model_lag_array[2,] = corrsim.lag_section_phase("Linear"	,	 0.25,  	 0.4, 		 2*np.pi/5, -2*np.pi/5)
	model_lag_array[3,] = corrsim.lag_section_phase("Linear"	,	  0.4,  	   5, 		         0,      np.pi)
	model_lag_array[4,] = corrsim.lag_section_phase("Polynomial",	    5,  	 200, 		     np.pi,      np.pi, \
		extra_freq = 28, extra_lag = 5*np.pi/2)

elif time_or_phase == "time":
	##	- Step 2: Set the overall lag.
	overall_lag		= -1E3		## Choose the overall (constant) lag for all non-defined sections

	##	- Step 3: Set the lag model using corrsim.lag_section_time.
	##	- NOTE: Time lags cannot go to or across zero. Please split distributions instead.
	##												distribution		start_freq,	close_freq,	start_lag,	close_lag=0
	model_lag_array[0,] = corrsim.lag_section_time("Linear"			,	0.001, 		0.4, 	    -1E3,		-1E-3)
	model_lag_array[1,] = corrsim.lag_section_time("Linear"			,	  0.4, 		0.8, 	    1E-6,		  0.2)
	model_lag_array[2,] = corrsim.lag_section_time("Linear"			,	  0.8, 		 10, 	     0.2,		  0.1)
	model_lag_array[3,] = corrsim.lag_section_time("Constant_Phase"	,	   10, 		200, 		2*np.pi)





##	===============			End of Input Parameters			===============
##	=======================================================================







print("\n--------------- Starting CorrSim ---------------")


##					Make Output Directory
##	--------------------------------------------------------

if not os.path.exists(output_dir):
    os.makedirs(output_dir)


##					Set-up Calculations
##	--------------------------------------------------------

mean_counts_A	= mean_counts_per_sec_A*time_resolution
mean_counts_B	= mean_counts_per_sec_B*time_resolution

obs_length	= length_of_observation
time_res	= time_resolution

time		= np.arange(0, obs_length, time_res)	## Create a set of time values
num_bins	= len(time)


## --- Optional: Optimise bins
## This is done to speed up calculations.
## If disabled, make sure your num_bins does not have a large prime factor (>4 digits).
## If optimise_bins = 2, it will adjust to a power of two. This is ideal for Fourier Analysis.
if optimize_bins == 2:
	powers_two	= 2**np.arange(40)
	num_bins	= powers_two[corrsim.place(num_bins, powers_two)]

	obs_length	= num_bins*time_res
	time		= np.arange(0, obs_length, time_res)	## Create a set of time values

	print("Adjusting to a power of 2. New observation length: ")
	print(str(obs_length) + "s (" + str(num_bins) + " (2^" + str(int(np.log2(num_bins))) + ") bins)")

## If optimise_bins = 7, it will adjust to a '7-smooth' number (i.e. no factor larger than 7).
## This balances speed of calculation with choice.
elif optimize_bins == 7:
	direction = "d"
	iterations = 1
	while corrsim.largest_prime_factor(num_bins) > 7:
		if direction == "d":
			num_bins -= iterations
			direction = "u"
			iterations += 1
		else:
			num_bins += iterations
			direction = "d"
			iterations += 1

	obs_length	= num_bins*time_res
	time		= np.arange(0, obs_length, time_res)	## Create a set of time values

	print("Adjusting to a 7-smooth number. New observation length: ")
	print(str(obs_length) + "s (" + str(num_bins) + " bins)")



## --- Check for a too-large number of bins

if num_bins > 1E6:

	print("")
	print("! ----- WARNING ----- !")
	print("Large number of bins detected!")
	print("(This warning occurs above 1e6 bins)")
	print("The program may break, or take a long time to complete.")
	print("")
	print("If you choose to continue, also be aware that this will")
	print("create large files (>100 MB) on your system!")
	print("")
	print("Please type y and then press enter to continue,")
	print("or any other key to quit CorrSim.")
	x = input()

	if x=="y":
		print("Continuing after warning regarding a large number of bins.")
	else:
	    print ("Quitting.\n")
	    sys.exit()


## --- Define frequencies

freq_min	= 1 / obs_length						## Minimum Frequency
freq_max	= 1 / (2*time_res)						## Maximum Frequency
freq_res	= 1 / obs_length						## Frequency Resolution


frequencies	= np.arange(0, freq_max+(freq_res*0.1), freq_res)

## In case of odd numbers of bins, make even instead.
if len(time) % 2 == 1:
	time		= time[0:-1]
	frequencies	= np.arange(0, freq_max, freq_res)



##		Power Spectrum Parameters
##	----------------------------------

print("Setting up model power spectra...")

if power_model_type == "broken_powerlaw":

	power_model = corrsim.power_model_broken_powerlaw(frequencies, ps_power, break_freq, coh_constant, coh_power, coh_break_freq)


elif power_model_type == "lorentzians":

	power_model = corrsim.power_model_lorentzians(frequencies, Lorentz_params_A, Lorentz_params_B)

	Lorentzians_A				= power_model[4]
	Lorentzians_B_coh			= power_model[5]
	Lorentzians_B_inc			= power_model[6]
	num_lorentz_A				= power_model[7]
	num_lorentz_B				= power_model[8]


unnorm_power_spectrum_A		= power_model[0]
unnorm_power_spectrum_B_coh	= power_model[1]
unnorm_power_spectrum_B_inc	= power_model[2]
coherence_model				= power_model[3]


##		Normalise Power Spectra
##	----------------------------------

print("Normalising power spectra...")

normalised_power_spectra	= corrsim.normalise_power_spectra(unnorm_power_spectrum_A, F_rms_A, unnorm_power_spectrum_B_coh, \
	unnorm_power_spectrum_B_inc, F_rms_B, obs_length, time_res, num_bins, mean_counts_A, mean_counts_B)

power_spectrum_A		= normalised_power_spectra[0]
power_spectrum_B		= normalised_power_spectra[1]
power_spectrum_B_coh	= normalised_power_spectra[2]
power_spectrum_B_inc	= normalised_power_spectra[3]
normalisation_A			= normalised_power_spectra[4]
frac_rms_norm_A			= normalised_power_spectra[5]
normalisation_B			= normalised_power_spectra[6]
frac_rms_norm_B			= normalised_power_spectra[7]

power_spectrum_model_A	= power_spectrum_A
power_spectrum_model_B	= power_spectrum_B


##			Add Red Noise
##	------------------------------


if apply_red == "A" or apply_red == "AB":
	print("Applying red noise to Band A...")
	power_spectrum_A		= corrsim.red_noise(frequencies, power_spectrum_A, mean_counts_A,  num_bins)

if apply_red == "B" or apply_red == "AB":
	print("Applying red noise to Band B...")
	power_spectrum_B_coh	= corrsim.red_noise(frequencies, power_spectrum_B_coh, mean_counts_B,  num_bins)
	power_spectrum_B_inc	= corrsim.red_noise(frequencies, power_spectrum_B_inc, mean_counts_B,  num_bins)



##		Convert Power Spectra to Amplitudes and Arguments
##	--------------------------------------------------------------

print("Creating Amplitudes, Arguments and Lags...")

amplitudes_arguments = corrsim.amplitudes_and_arguments(power_spectrum_A, power_spectrum_B_coh, power_spectrum_B_inc, \
	mean_counts_A, mean_counts_B, num_bins, frequencies, model_lag_array, overall_lag)

amplitude_A			= amplitudes_arguments[0]
amplitude_B_coh		= amplitudes_arguments[1]
amplitude_B_inc		= amplitudes_arguments[2]
arguments_A			= amplitudes_arguments[3]
arguments_B_coh		= amplitudes_arguments[4]
arguments_B_inc		= amplitudes_arguments[5]
model_lags			= amplitudes_arguments[6]

## Whichever lags are given (phase/time), create the other.
if time_or_phase == 'time':
	model_time_lags		= model_lags
	model_phase_lags	= []
	for j in range(len(model_lags)):
		model_phase_lags.append(model_lags[j] * 2 * np.pi * frequencies[j])

elif time_or_phase == 'phase':
	model_time_lags		= []
	model_phase_lags	= model_lags

	model_time_lags.append(np.nan)
	for j in range(1, len(model_lags)):
		model_time_lags.append(model_lags[j] / (2 * np.pi * frequencies[j]))


##          Calculate Model CCF
##  ----------------------------------

print("Calculating Model CCF...")

model_ccf_full = corrsim.calc_model_CCF(coherence_model, power_spectrum_model_A, frac_rms_norm_A, \
	power_spectrum_model_B, frac_rms_norm_B, model_lags, time_resolution, F_rms_A, F_rms_B)

model_ccf_lags	= model_ccf_full[0]
model_ccf		= model_ccf_full[1]


##          Convert Power Spectra to Lightcurve
##  ---------------------------------------------------

print("Converting Power Spectra to Lightcurve...")

lightcurves = corrsim.ps_to_lightcurve(amplitude_A, amplitude_B_coh, amplitude_B_inc, \
	arguments_A, arguments_B_coh, arguments_B_inc)

flux_A	= lightcurves[0]
flux_B	= lightcurves[1]

print("Given F_rms_A = ", F_rms_A)
print("Given F_rms_B = ", F_rms_B)
print("[Pre-noise] F_rms_A = std(flux_A) / mean(flux_A) = {:0.3f}".format(np.std(flux_A)/np.mean(flux_A)))
print("[Pre-noise] F_rms_B = std(flux_B) / mean(flux_B) = {:0.3f}".format(np.std(flux_B)/np.mean(flux_B)))


##         Add Noise
##  -----------------------

## --- Poisson Noise

if apply_poisson == "A" or apply_poisson == "AB":
	print("Applying Poisson Noise to Band A...")
	flux_A = corrsim.apply_poisson_noise(flux_A)

if apply_poisson == "B" or apply_poisson == "AB":
	print("Applying Poisson Noise to Band B...")
	flux_B = corrsim.apply_poisson_noise(flux_B)


## --- Scintillation Noise

scin_noise_A = 0
if apply_scintillation == "A" or apply_scintillation == "AB":
	print("Applying Scintillation Noise to Band A...")

	zenith_distance_A = ( (90-target_altitude_A) * 2*np.pi) / 360	#Convert to Zenith Distance in Radians

	##									An increase in this value...
	C_Y			= empirical_value_A		##	Increases
	D			= telescope_diameter_A	##	Decreases
	t			= exposure_time_A		##	Decreases
	gamma		= zenith_distance_A		##	Increases
	h_obs		= telescope_altitude_A	##	Decreases
	H			= turbulence_height_A	##	Increases
	##									Scintillation noise.

	## ^ in each variable...		   ^       v           v				  ^							v    ^		...scintillation noise
	scin_noise_A	= 10 * 10**(-6) * C_Y**2 * D**(-4/3) * t**(-1) * np.cos(gamma)**(-3) * np.exp((-2*h_obs)/H)	## Osborn+2015, Eqn. 7

	flux_A = corrsim.apply_scintillation_noise(scin_noise_A, flux_A)

scin_noise_B = 0
if apply_scintillation == "B" or apply_scintillation == "AB":
	print("Applying Scintillation Noise to Band B...")

	zenith_distance_B = ( (90-target_altitude_B) * 2*np.pi) / 360	#Convert to Zenith Distance in Radians

	C_Y			= empirical_value_B
	D			= telescope_diameter_B
	t			= exposure_time_B
	gamma		= zenith_distance_B
	h_obs		= telescope_altitude_B
	H			= turbulence_height_B

	scin_noise_B	= 10 * 10**(-6) * C_Y**2 * D**(-4/3) * t**(-1) * np.cos(gamma)**(-3) * np.exp((-2*h_obs)/H)	## Osborn+2015, Eqn. 7

	flux_B = corrsim.apply_scintillation_noise(scin_noise_B, flux_B)


## --- Readout Noise

if apply_readout == "A" or apply_readout == "AB":
	print("Applying Readout Noise to Band A...")
	flux_A			= corrsim.apply_readout_noise(read_noise_A, flux_A)

if apply_readout == "B" or apply_readout == "AB":
	print("Applying Readout Noise to Band B...")
	flux_B			= corrsim.apply_readout_noise(read_noise_B, flux_B)


flux_A[flux_A<0] = 0
flux_B[flux_B<0] = 0

print("[Post-noise] F_rms_A = std(flux_A) / mean(flux_A) = {:0.3f}".format(np.std(flux_A)/np.mean(flux_A)))
print("[Post-noise] F_rms_B = std(flux_B) / mean(flux_B) = {:0.3f}".format(np.std(flux_B)/np.mean(flux_B)))


##          Convert Lightcurve to Power Spectra
##  -------------------------------------------------------

if calculate_recovered_ps == "y":
	print("Converting Lightcurves to Power Spectra...")

	freqsarr	= corrsim.power_spectrum_recovery(flux_A, flux_B, num_bins, obs_length, frac_rms_norm_A, frac_rms_norm_B)

if (average_ps_iterations > 1):
	freqs = corrsim.averaged_power_spectrum_recovery(frequencies, mean_counts_A, num_bins, power_spectrum_model_A, frac_rms_norm_A, \
		average_ps_iterations, apply_red, apply_poisson, apply_scintillation, apply_readout, scin_noise_A, read_noise_A)

	freqsav			= freqs[0]
	freqsarr_array	= freqs[1]




##			Write the Data
##	----------------------------------

print("Writing Data...")

## Model Fourier Components
fileOut = "./" + output_dir + "/" + fileprefix + "Model Fourier Data.txt"
output = pd.DataFrame({	'Frequencies' : frequencies, 'Band_A_Power' : power_spectrum_model_A*frac_rms_norm_A,
						'Band_B_Power' : power_spectrum_model_B*frac_rms_norm_B, 'Coherence' : coherence_model,
						'Phase Lags' : model_phase_lags,
						'Time Lags' : model_time_lags})
output.to_csv(fileOut, index=False)

## Power Spectra
fileOut = "./" + output_dir + "/" + fileprefix + "Power Spectrum Data.txt"
output = pd.DataFrame({'Frequencies' : frequencies, 'Band_A_Power' : power_spectrum_A, 'Band_B_Power' : power_spectrum_B})
output.to_csv(fileOut, index=False)

## CCF
fileOut = "./" + output_dir + "/" + fileprefix + "Model CCF Data.txt"
output = pd.DataFrame({'model_ccf_lags' : model_ccf_lags, 'model_ccf' : model_ccf})
output.to_csv(fileOut, index=False)

## Lightcurves
fileOut = "./" + output_dir + "/" + fileprefix + "Lightcurve Data.txt"
output = pd.DataFrame({'A_Time' : time, 'Band_A_Flux' : flux_A, 'Band_B_Flux' : flux_B})
output.to_csv(fileOut, index=False)


##			Write a short log file of the Details
##	-----------------------------------------------------

fileOut = output_dir + "/" + fileprefix + "Log File.txt"

with open(fileOut, 'w') as the_file:
	the_file.write("File Prefix:\t\t" + fileprefix + '\n')
	the_file.write("Obs. Length:\t\t" + str(length_of_observation) + "s\n")
	the_file.write("Time Res:\t\t" + str(time_resolution) + "s\n")
	the_file.write("Poisson:\t\t" + apply_poisson + "\n")
	the_file.write("Scintillation:\t\t" + apply_scintillation + "\n")
	the_file.write("Readout:\t\t" + apply_readout + "\n")
	the_file.write("mean_counts_per_sec_A:\t" + str(mean_counts_per_sec_A) + "\n")
	the_file.write("mean_counts_per_sec_B:\t" + str(mean_counts_per_sec_B) + "\n")
	the_file.write("F_rms_A:\t\t" + str(F_rms_A) + "\n")
	the_file.write("F_rms_B:\t\t" + str(F_rms_B) + "\n")
	the_file.write("\n")
	the_file.write("Min Freq:\t\t" + str(freq_min) + "\n")
	the_file.write("Max Freq:\t\t" + str(freq_max) + "\n")
	the_file.write("Freq Res:\t\t" + str(freq_res) + "\n")
	the_file.write("\n")
	the_file.write("Frac_RMS_Norm_1:\t" + str(frac_rms_norm_A) + "\n")
	the_file.write("Frac_RMS_Norm_2:\t" + str(frac_rms_norm_B) + "\n")
	the_file.write("Returned F_rms_1:\t" + str(np.std(flux_A)/np.mean(flux_A)) + "\n")
	the_file.write("Returned F_rms_2:\t" + str(np.std(flux_B)/np.mean(flux_B)) + "\n")
	the_file.write("\n")
	the_file.write("Max Flux_1:\t\t" + str(max(flux_A)) + "\n")
	the_file.write("Min Flux_1:\t\t" + str(min(flux_A)) + "\n")
	the_file.write("Mean Flux_1:\t\t" + str(np.mean(flux_A)) + "\n")
	the_file.write("\n")
	the_file.write("Max Flux_2:\t\t" + str(max(flux_B)) + "\n")
	the_file.write("Min Flux_2:\t\t" + str(min(flux_B)) + "\n")
	the_file.write("Mean Flux_2:\t\t" + str(np.mean(flux_B)) + "\n")
	the_file.write("\n")
	the_file.write("Lag Method:\t\t" + time_or_phase + "\n")


###				Plotting
###	----------------------------------

###
### ===== Plot Power Spectrum =====
###

if plot_model_ps == "y":

	plt.figure(figsize=(10,5))

	plt.plot(frequencies,  power_spectrum_model_A*frac_rms_norm_A, linestyle="-", label="Band A", color="blue")
	plt.plot(frequencies,  power_spectrum_model_B*frac_rms_norm_B, linestyle="-", label="Band B", color="red")

	plt.legend()

	y_min = min([min((power_spectrum_B*frac_rms_norm_B)[1:len(frequencies)]), min((power_spectrum_A*frac_rms_norm_A)[1:len(frequencies)])])
	y_max = max([max((power_spectrum_B*frac_rms_norm_B)[1:len(frequencies)]), max((power_spectrum_A*frac_rms_norm_A)[1:len(frequencies)])])

	plt.xlim(frequencies[1]/2, max(frequencies)*2)
	plt.ylim(y_min/2, y_max*2)

	plt.xscale("log")
	plt.yscale("log")

	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Frac. RMS Power')

	plt.grid(linestyle=":", zorder=5)

	fileOut = "./" + output_dir + "/" + fileprefix + "Model Power Spectra.png"
	plt.savefig(fileOut, bbox_inches='tight')

	plt.close('all')


###
### ===== Plot Power Spectra Components =====
###


if plot_model_ps_comps == "y":

	plt.figure(figsize=(10,8))

	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Frequency * Frac. RMS Power')

	if power_model_type == "broken_powerlaw":

		plt.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*unnorm_power_spectrum_A[1:len(frequencies)] * normalisation_A, linestyle = '--', color="blue", alpha=0.5)

		plt.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*unnorm_power_spectrum_B_coh[1:len(frequencies)] * normalisation_B, linestyle = '--', alpha=0.5, color="purple")
		plt.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*unnorm_power_spectrum_B_inc[1:len(frequencies)] * normalisation_B, linestyle = ':',  alpha=0.5, color="red")

	elif power_model_type == "lorentzians":

		for i in range(num_lorentz_A):
			plt.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*Lorentzians_A[1:len(frequencies),i]*normalisation_A, linestyle = '--', color="blue", alpha=0.5)

		for i in range(num_lorentz_B):
			plt.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*Lorentzians_B_coh[1:len(frequencies),i]*normalisation_B,linestyle = '--', alpha=0.5, color="purple")
			plt.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*Lorentzians_B_inc[1:len(frequencies),i]*normalisation_B,linestyle = ':',  alpha=0.5, color="red")

	plt.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*power_spectrum_model_A[1:len(frequencies)] * frac_rms_norm_A, linewidth = 3, linestyle = '-', color="blue")
	plt.plot(frequencies[1:len(frequencies)], frequencies[1:len(frequencies)]*power_spectrum_model_B[1:len(frequencies)] * frac_rms_norm_B, linewidth = 3, linestyle = '-', color="red")

	line1 = mlines.Line2D([], [], color='blue',		linestyle='-',	label='A (Overall)')
	line2 = mlines.Line2D([], [], color='blue',		linestyle='--',	label='A (Components)')
	line3 = mlines.Line2D([], [], color='none',		linestyle='--',	label=' ')
	line4 = mlines.Line2D([], [], color='red',		linestyle='-',	label='B (Overall)')
	line5 = mlines.Line2D([], [], color='purple',	linestyle='--',	label='B (Coherent)')
	line6 = mlines.Line2D([], [], color='red',		linestyle=':',	label='B (Incoherent)')

	if power_model_type == "broken_powerlaw":
		plt.legend(handles=[line1, line3, line4, line5, line6])
	elif power_model_type == "lorentzians":
		plt.legend(handles=[line1, line2, line3, line4, line5, line6])

	plt.xscale("log")
	plt.yscale("log")

	plt.grid(linestyle=":", zorder=5, linewidth=0.5)

	fileOut = "./" + output_dir + "/" + fileprefix + "Power Spectrum Components.png"
	plt.savefig(fileOut, bbox_inches='tight')

	plt.close('all')


###
### ===== Plot Model Lags =====
###

if plot_model_lags == "y":

	plt.figure(figsize=(10,5))

	if time_or_phase == 'time':
		plt.plot(frequencies,  model_lags, linestyle="-", linewidth=2, color="darkviolet")
		plt.plot(frequencies, -model_lags, linestyle="-", linewidth=2, color="blue")
		plt.xlim(frequencies[1]/2, max(frequencies)*2)
		plt.ylim([min(abs(model_lags))/2, max(np.append(model_lags, -model_lags))*2])
		plt.ylabel('Time Lags')
		plt.yscale("log")

	elif time_or_phase == 'phase':
		plt.plot(frequencies, model_lags, linestyle="-", linewidth=2, color="darkviolet")
		plt.xlim(frequencies[1]/2, max(frequencies)*2)
		tick_pos= np.arange(-2*np.pi, 4*np.pi , np.pi)
		labels = ['-$2\pi$', '-$\pi$', '0', '$\pi$', '$2\pi$', '$3\pi$']
		plt.yticks(tick_pos, labels)
		plt.ylabel('Phase Lags')
		plt.hlines(0, frequencies[1]/2, max(frequencies)*2, linestyle="--", linewidth=1, zorder=9)

	plt.xscale("log")
	plt.xlabel('Frequencies')
	plt.grid(linestyle=":", zorder=5)

	fileOut = "./" + output_dir + "/" + fileprefix + "Model Lags (" + time_or_phase + ").png"
	plt.savefig(fileOut, bbox_inches='tight')

	plt.close('all')

###
### ===== Plot Model CCF =====
###

if plot_model_ccf == "y":

	plt.figure(figsize=(8,5))

	plt.plot(model_ccf_lags, model_ccf, color="k")

	plt.xlabel('Lag (s)')
	plt.ylabel('CCF Coefficient')

	plt.xlim(-10,10)

	plt.grid(linestyle=":", zorder=5)

	plt.axhline(0, linestyle="--", linewidth=1, zorder=9)
	plt.axvline(0, linestyle="--", linewidth=1, zorder=9)

	fig = plt.gcf()

	savename = "./" + output_dir + "/" + fileprefix + "Model CCF (10s lag).png"
	fig.savefig(savename, bbox_inches='tight')

	plt.xlim(-0.5,0.5)

	savename = "./" + output_dir + "/" + fileprefix + "Model CCF (2s lag).png"
	plt.savefig(savename, bbox_inches='tight')



###
### ===== Plot Lightcurves =====
###

if plot_lightcurves == "y":

	insetA_1 = 0
	insetA_2 = 9.5

	insetB_1 = 2
	insetB_2 = 3

	fig = plt.figure(figsize=(10,5))

	plot_margin = 0.1




	## --- Main (Band A) ---
	left = 0.0
	bottom = 0.755
	width = 1.0
	height = 0.245
	main_A_ax = fig.add_axes([left, bottom, width, height], [])

	main_A_ax.plot(corrsim.ma(time, 50), corrsim.ma(flux_A, 50), linestyle='-', lw=1, color="blue")

	main_A_ax.set_xlabel('Time (s)')
	main_A_ax.set_ylabel('Counts/Bin')

	main_A_ax.xaxis.tick_top()
	main_A_ax.xaxis.set_label_position("top")

	main_A_ax.grid(linestyle=":", zorder=5)

	main_A_ax.axvline(insetA_1, linestyle="--", linewidth=1, color="blue", zorder=9)
	main_A_ax.axvline(insetA_2, linestyle="--", linewidth=1, color="blue", zorder=9)

	## --- Main (Band B) ---
	left = 0.0
	bottom = 0.51
	width = 1.0
	height = 0.245
	main_B_ax = fig.add_axes([left, bottom, width, height], [])

	main_B_ax.plot(corrsim.ma(time, 50), corrsim.ma(flux_B, 50), linestyle='-', lw=1, color="red")

	main_B_ax.set_ylabel('Counts/Bin')
	main_B_ax.xaxis.set_ticklabels([])

	main_B_ax.yaxis.tick_right()
	main_B_ax.yaxis.set_label_position("right")

	main_B_ax.grid(linestyle=":", zorder=5)

	main_B_ax.axvline(insetA_1, linestyle="--", linewidth=1, color="blue", zorder=9)
	main_B_ax.axvline(insetA_2, linestyle="--", linewidth=1, color="blue", zorder=9)


	## --- Zoom 1 (Band A) ---
	left = 0.0
	bottom = 0.245
	width = 0.69
	height = 0.245
	zoomA_A_ax = fig.add_axes([left, bottom, width, height], [])

	zoomA_A_ax.plot(time, flux_A, linestyle='-', lw=1, color="blue")

	zoomA_A_ax.set_xlim([insetA_1, insetA_2])
	zoomA_A_ax.xaxis.tick_top()
	zoomA_A_ax.xaxis.set_ticklabels([])

	zoomA_A_ax.set_ylabel('Counts/Bin')

	zoomA_A_ax.grid(linestyle=":", zorder=5)

	zoomA_A_ax.axvline(insetB_1, linestyle="--", linewidth=1, color="blue", zorder=9)
	zoomA_A_ax.axvline(insetB_2, linestyle="--", linewidth=1, color="blue", zorder=9)

	## --- Zoom 1 (Band B) ---
	left = 0.0
	bottom = 0.0
	width = 0.69
	height = 0.245
	zoomA_B_ax = fig.add_axes([left, bottom, width, height], [])

	zoomA_B_ax.plot(time, flux_B, linestyle='-', lw=1, color="red")

	zoomA_B_ax.set_xlim([insetA_1, insetA_2])

	zoomA_B_ax.set_xlabel('Time (s)')
	zoomA_B_ax.set_ylabel('Counts/Bin')

	zoomA_B_ax.grid(linestyle=":", zorder=5)

	zoomA_B_ax.axvline(insetB_1, linestyle="--", linewidth=1, color="blue", zorder=9)
	zoomA_B_ax.axvline(insetB_2, linestyle="--", linewidth=1, color="blue", zorder=9)


	## --- Zoom 2 (Band A) ---
	left = 0.71
	bottom = 0.245
	width = 0.29
	height = 0.245
	zoomB_A_ax = fig.add_axes([left, bottom, width, height], [])

	zoomB_A_ax.plot(time, flux_A, linestyle='-', lw=1, color="blue")

	zoomB_A_ax.set_xlim([insetB_1, insetB_2])
	zoomB_A_ax.xaxis.tick_top()
	zoomB_A_ax.xaxis.set_ticklabels([])

	zoomB_A_ax.yaxis.tick_right()
	zoomB_A_ax.yaxis.set_label_position("right")
	zoomB_A_ax.set_ylabel('Counts/Bin')

	zoomB_A_ax.grid(linestyle=":", zorder=5)


	## --- Zoom 2 (Band B) ---
	left = 0.71
	bottom = 0.0
	width = 0.29
	height = 0.245
	zoomB_B_ax = fig.add_axes([left, bottom, width, height], [])

	zoomB_B_ax.plot(time, flux_B, linestyle='-', lw=1, color="red")

	zoomB_B_ax.set_xlim([insetB_1, insetB_2])

	zoomB_B_ax.yaxis.tick_right()
	zoomB_B_ax.yaxis.set_label_position("right")
	zoomB_B_ax.set_xlabel('Time (s)')
	zoomB_B_ax.set_ylabel('Counts/Bin')

	zoomB_B_ax.grid(linestyle=":", zorder=5)



	fileOut = "./" + output_dir + "/" + fileprefix + "Lightcurves.png"
	plt.savefig(fileOut, bbox_inches='tight')

	plt.close('all')


###
### ===== Plot Recovered Power Spectrum =====
###

if calculate_recovered_ps == "y":

	plt.figure(figsize=(10,5))

	plt.step(freqsarr[1:-1,0], freqsarr[1:-1,1], where="mid", label="A (Recovered)", color="turquoise")
	plt.step(freqsarr[1:-1,0], freqsarr[1:-1,2], where="mid", label="B (Recovered)", color="coral")

	plt.plot(frequencies,  power_spectrum_model_A*frac_rms_norm_A, linestyle="-", label="A (Model)", color="blue")
	plt.plot(frequencies,  power_spectrum_model_B*frac_rms_norm_B, linestyle="-", label="B (Model)", color="red")

	plt.legend()

	line1 = mlines.Line2D([], [], color='blue',			linestyle='-',	label='A (Model)')
	line2 = mlines.Line2D([], [], color='turquoise',	linestyle='-',	label='A (Recovered)')
	line3 = mlines.Line2D([], [], color='none',			linestyle='-',	label=' ')
	line4 = mlines.Line2D([], [], color='red',			linestyle='-',	label='B (Model)')
	line5 = mlines.Line2D([], [], color='coral',		linestyle='-',	label='B (Recovered)')

	plt.legend(handles=[line1, line2, line3, line4, line5])

	plt.xlim(frequencies[1]/2, max(frequencies)*2)
	plt.ylim(min(freqsarr[1:len(freqsarr),2])/2, max(freqsarr[1:len(freqsarr),1])*2)

	plt.xscale("log")
	plt.yscale("log")

	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Frac. RMS Power')

	plt.grid(linestyle=":", zorder=5)

	fileOut = "./" + output_dir + "/" + fileprefix + "Power Spectrum Recovered.png"
	plt.savefig(fileOut, bbox_inches='tight')

	plt.close('all')

###
### ===== Plot Averaged Power Spectrum =====
###

if (average_ps_iterations > 1):
	plt.figure(figsize=(10,5))

	plt.step(freqsav[1:-1,0], freqsav[1:-1,1], where="mid", color="blue")

	plt.xlim(frequencies[1]/2, max(frequencies)*2)
	plt.ylim(min(freqsav[1:-1,1])/2, max(freqsav[1:-1,1])*2)

	plt.xscale("log")
	plt.yscale("log")

	plt.xlabel('Frequency (Hz)')
	plt.ylabel('Frac. RMS Power')

	plt.grid(linestyle=":", zorder=5)

	fileOut = "./" + output_dir + "/" + fileprefix + "Power Spectrum Averaged.png"
	plt.savefig(fileOut, bbox_inches='tight')

	plt.close('all')


###
### ===== Calculate Recovered CCF =====
###

bin_kwarg = ""
if recovered_ccf_binning > 1:
	bin_kwarg = " (Binned " + str(recovered_ccf_binning) + ")"

if calculate_recovered_ccf == "y":

	print("Calculating Recovered CCF...")

	ccf_av = corrsim.recovered_ccf(flux_A, flux_B, time_res, recovered_ccf_segment, recovered_ccf_binning)

	lower = int(len(ccf_av)/3)
	upper = int(2*len(ccf_av)/3)
	mean_ccf_error = np.mean(ccf_av[lower:upper,2])
	stdv_ccf_error = np.std(ccf_av[lower:upper,2])

	print(" ")
	print("------------------------------")
	print("Mean CCF Error: ", '{:g}'.format(float('{:.{p}g}'.format(mean_ccf_error, p=3))), " +/- ", '{:g}'.format(float('{:.{p}g}'.format(stdv_ccf_error, p=3))))
	print("------------------------------")
	print(" ")

	## Write the data
	fileOut = "./" + output_dir + "/" + fileprefix + "Recovered CCF Data (" + str(recovered_ccf_segment) + "s" + bin_kwarg + ").txt"# + bin_kwarg
	output = pd.DataFrame({'lag' : ccf_av[:,0], 'ccf' : ccf_av[:,1], 'ccf_err' : ccf_av[:,2]})
	output.to_csv(fileOut, index=False)


###
### ===== Plot Recovered CCF =====
###

if calculate_recovered_ccf == "y":

	plt.figure(figsize=(10,5))

	plt.step(ccf_av[:,0], ccf_av[:,1], where='mid', lw=1, color="black")

	plt.xlim(-recovered_ccf_segment/3, recovered_ccf_segment/3)

	plt.xlabel('Lag (s)')
	plt.ylabel('CCF Coefficient')

	plt.grid(linestyle=":", zorder=5)

	fileOut = "./" + output_dir + "/" + fileprefix + "Averaged CCF" + bin_kwarg + ".png"
	plt.savefig(fileOut, bbox_inches='tight')

	plt.close('all')


	plt.figure(figsize=(10,5))

	plt.step(ccf_av[:,0], ccf_av[:,1], where='mid', lw=1, color="red")
	plt.plot(model_ccf_lags, model_ccf, color="k")

	plt.xlim(-recovered_ccf_segment/3, recovered_ccf_segment/3)

	plt.xlabel('Lag (s)')
	plt.ylabel('CCF Coefficient')

	plt.grid(linestyle=":", zorder=5)

	fileOut = "./" + output_dir + "/" + fileprefix + "Averaged CCF + Model (" + str(recovered_ccf_segment) + "s" + bin_kwarg + ").png"
	plt.savefig(fileOut, bbox_inches='tight')

	plt.close('all')


###
### ===== Calculate Coherence =====
###

if calculate_fourier == "y":

	print("Beginning Coherence Calculations...")

	segment_size = time_res * fourier_bins

	print(str(int(np.floor(num_bins/fourier_bins))) + " segments to be used, " + str(fourier_bins) + " bins (" + str(segment_size) + " seconds) each.")

	fourier_data = corrsim.fourier(time, flux_A, flux_B, time_resolution, \
		segment_size, fourier_rebin, reference_freq, apply_poisson, remove_whitenoise)

	fourier_output = np.transpose(fourier_data)

	fileOut = "./" + output_dir + "/" + fileprefix + "Coherence Data (bins " + str(fourier_bins) + " rebin " + str(fourier_rebin) + ").txt"
	output = pd.DataFrame(fourier_output)
	output.to_csv(fileOut, index=False, header=False)

	fourier_freqs			= fourier_data[0]
	powerspectra_A			= fourier_data[1]
	powerspectra_A_err		= fourier_data[2]
	powerspectra_B			= fourier_data[3]
	powerspectra_B_err		= fourier_data[4]
	coherence				= fourier_data[5]
	coherence_err			= fourier_data[6]
	phase_lags				= fourier_data[7]
	phase_lags_err			= fourier_data[8]
	time_lags				= fourier_data[9]
	time_lags_inv			= fourier_data[10]
	time_lags_err			= fourier_data[11]

	print(" ")
	print("-------------------------------")

	print("Median Absolute Fourier Errors (as percentage of measured values):")

	print("Powerspectrum A:\t",	round(np.nanmedian(abs((powerspectra_A_err/powerspectra_A)*100)), 				2),	"%", sep="")
	print("Powerspectrum B:\t",	round(np.nanmedian(abs((powerspectra_B_err/powerspectra_B)*100)), 				2),	"%", sep="")
	print("Coherence:\t\t",		round(np.nanmedian(abs((coherence_err/coherence)*100)), 						2),	"%", sep="")
	print("Phase Lags:\t\t",	round(np.nanmedian(abs((np.array(phase_lags_err)/np.array(phase_lags))*100)),	2),	"%", sep="")
	print("Time Lags:\t\t",		round(np.nanmedian(abs((np.array(time_lags_err) /np.array(time_lags)) *100)),	2),	"%", sep="")

	print("-------------------------------")
	print(" ")

	fig, axs = plt.subplots(nrows=4, ncols=1, sharex=True, figsize=(5,8), gridspec_kw={'height_ratios': [10,7,7,10]})

	## --- Power Spectra ---

	ax = axs[0]

	ax.errorbar(fourier_freqs, fourier_freqs * powerspectra_A, yerr=fourier_freqs * powerspectra_A_err, \
		ms=4, lw=0.5, capsize=3, fmt='bo', zorder=9)
	ax.errorbar(fourier_freqs, fourier_freqs * powerspectra_B, yerr=fourier_freqs * powerspectra_B_err, \
		ms=4, lw=0.5, capsize=3, fmt='ro', zorder=9)

	ax.set_ylabel("Freq * RMS Squared")
	ax.set_yscale('log', nonpositive='clip')
	ax.grid(linestyle='--')

	ax.text(reference_freq, max(fourier_freqs * powerspectra_A),	"Band A", horizontalalignment='center', \
		verticalalignment='center', style='italic', color='blue')
	ax.text(reference_freq, max(fourier_freqs * powerspectra_B),  "Band B", horizontalalignment='center', \
		verticalalignment='center', style='italic', color='red')


	## --- Coherence ---
	ax = axs[1]
	ax.set_xscale('log')
	ax.set_yscale('log', nonpositive="clip")
	ax.set_ylim([min(coherence)/2, 2])

	ax.errorbar(fourier_freqs, coherence, yerr=coherence_err, lw=0.5, fmt='mo', zorder=9)
	ax.set_ylabel("Coherence")
	ax.grid(linestyle='--')


	## --- Phase Lags ---
	ax = axs[2]
	ax.set_xscale('log')

	ax.yaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
	ax.yaxis.set_minor_locator(plt.MultipleLocator(np.pi / 4))
	ax.yaxis.set_major_formatter(plt.FuncFormatter(corrsim.format_func))
	ax.set_ylim([-3.8, 3.8])

	ax.errorbar(fourier_freqs, phase_lags, yerr=phase_lags_err, lw=0.5, fmt='mo')

	ax.set_ylabel("Phase Lag (Radians)")
	ax.grid(linestyle='--')


	## --- Time Lags ---
	ax = axs[3]
	ax.set_xscale('log')
	ax.set_yscale('log', nonpositive="clip")


	ax.errorbar(fourier_freqs, time_lags,     yerr=time_lags_err, lw=0.5, fmt='mo', label = "+ve Lags")
	ax.errorbar(fourier_freqs, time_lags_inv, yerr=time_lags_err, lw=0.5, fmt='mo', fillstyle = "none", label = "-ve Lags")

	ax.legend()

	ax.set_ylabel("Time Lag (s)")
	ax.set_xlabel("Frequency (Hz)")
	ax.grid(linestyle='--')

	## ----- PLOT END -----

	plt.subplots_adjust(bottom=0.1, left=0.15, top=0.98, right=0.98, hspace=0)

	savename = "./" + output_dir + "/" + fileprefix + "Coherence.pdf"

	plt.savefig(savename, bbox_inches='tight')

	plt.close('all')


if plot_fourier_models == "y":

	fig, axs = plt.subplots(nrows=4, ncols=1, sharex=True, figsize=(5,8), gridspec_kw={'height_ratios': [10,7,7,10]})

	## --- Power Spectra ---

	ax = axs[0]

	ax.errorbar(fourier_freqs, fourier_freqs * powerspectra_A, yerr=fourier_freqs * powerspectra_A_err, \
		ms=4, lw=0.5, capsize=3, fmt='bo', zorder=9)
	ax.errorbar(fourier_freqs, fourier_freqs * powerspectra_B, yerr=fourier_freqs * powerspectra_B_err, \
		ms=4, lw=0.5, capsize=3, fmt='ro', zorder=9)
	ax.plot(frequencies,	frequencies*power_spectrum_model_A*frac_rms_norm_A,	linestyle="-", zorder =14, color="black")
	ax.plot(frequencies,	frequencies*power_spectrum_model_B*frac_rms_norm_B,	linestyle=":", zorder =14, color="black")

	ax.set_ylabel("Freq * RMS Squared")
	ax.set_yscale('log', nonpositive='clip')
	ax.grid(linestyle='--')

	ax.text(reference_freq, max(fourier_freqs * powerspectra_A),	"Band A", horizontalalignment='center', \
		verticalalignment='center', style='italic', color='blue')
	ax.text(reference_freq, max(fourier_freqs * powerspectra_B),  "Band B", horizontalalignment='center', \
		verticalalignment='center', style='italic', color='red')


	## --- Coherence ---
	ax = axs[1]
	ax.set_xscale('log')
	ax.set_yscale('log', nonpositive="clip")
	ax.set_ylim([min(coherence)/2, 2])

	ax.errorbar(fourier_freqs, coherence, yerr=coherence_err, lw=0.5, fmt='mo', zorder=9)
	ax.plot(frequencies, coherence_model, linestyle="-", zorder =14, color="black")

	ax.set_ylabel("Coherence")
	ax.grid(linestyle='--')


	## --- Phase Lags ---
	ax = axs[2]
	ax.set_xscale('log')

	ax.yaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
	ax.yaxis.set_minor_locator(plt.MultipleLocator(np.pi / 4))
	ax.yaxis.set_major_formatter(plt.FuncFormatter(corrsim.format_func))
	ax.set_ylim([-3.8, 3.8])

	ax.errorbar(fourier_freqs, phase_lags, yerr=phase_lags_err, lw=0.5, fmt='mo')
	ax.plot(frequencies, model_phase_lags-(4*np.pi), lw=1, zorder=8, linestyle='-', color="k")
	ax.plot(frequencies, model_phase_lags-(2*np.pi), lw=1, zorder=8, linestyle='-', color="k")
	ax.plot(frequencies, model_phase_lags, lw=1, zorder=8, linestyle='-', color="k")
	ax.plot(frequencies, model_phase_lags+(2*np.pi), lw=1, zorder=8, linestyle='-', color="k")
	ax.plot(frequencies, model_phase_lags+(4*np.pi), lw=1, zorder=8, linestyle='-', color="k")

	ax.set_ylabel("Phase Lag (Radians)")
	ax.grid(linestyle='--')


	## --- Time Lags ---
	ax = axs[3]
	ax.set_xscale('log')
	ax.set_yscale('log', nonpositive="clip")


	ax.errorbar(fourier_freqs, time_lags,     yerr=time_lags_err, lw=0.5, fmt='mo', label = "+ve Lags")
	ax.errorbar(fourier_freqs, time_lags_inv, yerr=time_lags_err, lw=0.5, fmt='bo', label = "-ve Lags")

	ax.plot(frequencies,  np.array(model_time_lags), lw=1, zorder=8, linestyle='-', color="k")
	ax.plot(frequencies, -np.array(model_time_lags), lw=1, zorder=8, linestyle='--', color="k")

	ax.legend()

	ax.set_ylabel("Time Lag (s)")
	ax.set_xlabel("Frequency (Hz)")
	ax.grid(linestyle='--')

	## ----- PLOT END -----

	plt.subplots_adjust(bottom=0.1, left=0.15, top=0.98, right=0.98, hspace=0)

	savename = "./" + output_dir + "/" + fileprefix + "Coherence+Models.pdf"

	plt.savefig(savename, bbox_inches='tight')

	plt.close('all')





if plot_compilation == "y" and calculate_recovered_ccf == "n":

	print('WARNING: Compilation plot requested without CCF calculations given. Please define calculate_recovered_ccf = "y"')

elif plot_compilation == "y" and calculate_fourier == "n":

	print('WARNING: Compilation plot requested without Fourier calculations given. Please define calculate_fourier = "y"')

elif plot_compilation == "y" and calculate_recovered_ccf == "y" and calculate_fourier == "y":

	fig = plt.figure(figsize=(6, 13))
	plot_margin = 0.1

	## ----- Lightcurves -----
	insetA_1 = 0
	insetA_2 = 100

	insetB_1 = 5
	insetB_2 = 14.8

	insetC_1 = 9
	insetC_2 = 11

	## --- Inset 1 ---
	## Band A
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.0, 0.94,  1.0, 0.06], [])

	ax.plot(corrsim.ma(time, 50), corrsim.ma(flux_A, 50), linestyle='-', lw=1, color="blue")
	ax.set_xlim([insetA_1, insetA_2])

	ax.minorticks_on()
	ax.xaxis.tick_top()
	ax.xaxis.set_label_position("top")
	ax.set_xlabel('Time (s)')
	ax.set_ylabel('Counts')

	ax.grid(linestyle=":", zorder=5)

	ax.axvline(insetB_1, linestyle="--", linewidth=1, color="blue", zorder=9)
	ax.axvline(insetB_2, linestyle="--", linewidth=1, color="blue", zorder=9)

	## Band B
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.0, 0.88,  1.0, 0.06], [])

	ax.plot(corrsim.ma(time, 50), corrsim.ma(flux_B, 50), linestyle='-', lw=1, color="red")
	ax.set_xlim([insetA_1, insetA_2])

	ax.minorticks_on()
	ax.xaxis.set_ticklabels([])
	ax.yaxis.tick_right()
	ax.yaxis.set_label_position("right")
	ax.set_ylabel('Counts')

	ax.grid(linestyle=":", zorder=5)

	ax.axvline(insetB_1, linestyle="--", linewidth=1, color="blue", zorder=9)
	ax.axvline(insetB_2, linestyle="--", linewidth=1, color="blue", zorder=9)


	## --- Inset 2 ---
	## Band A
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.0, 0.80, 0.49, 0.06], [])
	ax.plot(time, flux_A, linestyle='-', lw=1, color="blue")
	ax.set_xlim([insetB_1, insetB_2])

	ax.minorticks_on()
	ax.xaxis.tick_top()
	ax.xaxis.set_ticklabels([])
	ax.set_ylabel('Counts')

	ax.grid(linestyle=":", zorder=5)

	ax.axvline(insetC_1, linestyle="--", linewidth=1, color="blue", zorder=9)
	ax.axvline(insetC_2, linestyle="--", linewidth=1, color="blue", zorder=9)

	## Band B
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.0,  0.74,  0.49, 0.06], [])
	ax.plot(time, flux_B, linestyle='-', lw=1, color="red")
	ax.set_xlim([insetB_1, insetB_2])

	ax.minorticks_on()
	ax.set_xlabel('Time (s)')
	ax.set_ylabel('Counts')

	ax.grid(linestyle=":", zorder=5)

	ax.axvline(insetC_1, linestyle="--", linewidth=1, color="blue", zorder=9)
	ax.axvline(insetC_2, linestyle="--", linewidth=1, color="blue", zorder=9)


	## --- Inset 3 ---
	## Band A
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.51, 0.80, 0.49, 0.06], [])
	ax.plot(time, flux_A, linestyle='-', lw=1, color="blue")
	ax.set_xlim([insetC_1, insetC_2])

	ax.minorticks_on()
	ax.xaxis.tick_top()
	ax.xaxis.set_ticklabels([])
	ax.yaxis.tick_right()
	ax.yaxis.set_label_position("right")
	ax.set_ylabel('Counts')

	ax.grid(linestyle=":", zorder=5)

	## Band B
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.51,  0.74,  0.49, 0.06], [])
	ax.plot(time, flux_B, linestyle='-', lw=1, color="red")
	ax.set_xlim([insetC_1, insetC_2])

	ax.minorticks_on()
	ax.yaxis.tick_right()
	ax.yaxis.set_label_position("right")
	ax.set_xlabel('Time (s)')
	ax.set_ylabel('Counts')

	ax.grid(linestyle=":", zorder=5)




	## ----- Compilation: Y limits and Boxes -----


	ax = fig.add_axes([ 0.0,  0.66,  0.48, 0.02], [])
	ax.axis('off')
	ax.text(0.5, 1.0, "--- Inputs ---", horizontalalignment='center', verticalalignment='center', fontsize=14)

	ax = fig.add_axes([0.52,  0.66,  0.48, 0.02], [])
	ax.axis('off')
	ax.text(0.5, 1.0, "--- Outputs ---", horizontalalignment='center', verticalalignment='center', fontsize=14)

	## --- Power Spectra ---
	ps_ylimits	= [
		min([
			  min(power_spectrum_model_A[1:len(power_spectrum_model_A)]*frac_rms_norm_A)
			, min(power_spectrum_model_B[1:len(power_spectrum_model_B)]*frac_rms_norm_B)
#			, min(fourier_freqs * powerspectra_A)
		])*0.5,
		max([
			  max(power_spectrum_model_A[1:len(power_spectrum_model_A)]*frac_rms_norm_A)
			, max(power_spectrum_model_B[1:len(power_spectrum_model_B)]*frac_rms_norm_B)
#			, max(fourier_freqs * powerspectra_A)
		])*1.5
		]

	ps_rect = patches.Rectangle(
		( min(fourier_freqs)*0.7
		, ps_ylimits[0])
		, (max(fourier_freqs)-min(fourier_freqs))*1.3
		, ps_ylimits[1]-ps_ylimits[0]
		, linewidth=1, linestyle='--', edgecolor='k', facecolor='none')

	## --- Coherence ---
	coh_ylimits	= [
		min([
			min(coherence_model[1:len(coherence_model)]),
			min(coherence)
		])*0.7,
		max([
			max(coherence_model[1:len(coherence_model)]),
			max(coherence)
		])*1.3]

	coh_rect = patches.Rectangle(
		( min(fourier_freqs)*0.7
		, coh_ylimits[0])
		, (max(fourier_freqs)-min(fourier_freqs))*1.3
		, coh_ylimits[1]-coh_ylimits[0]
		, linewidth=1, linestyle='--', edgecolor='k', facecolor='none')

	## --- Phase Lags ---
	phase_rect = patches.Rectangle(
		( min(fourier_freqs)*0.7
		, -np.pi)
		, (max(fourier_freqs)-min(fourier_freqs))*1.3
		, 2*np.pi
		, linewidth=1, linestyle='--', edgecolor='k', facecolor='none')

	## --- Time Lags ---
	if max(time_lags_inv) > 0:
		time_ylimits	= [
			np.nanmin([
				min([i for i in time_lags if i > 0] if max(time_lags) > 0 else [np.nan]),
				min(i for i in time_lags_inv if i > 0)
			])*0.7,
			np.nanmax([
				max([i for i in time_lags if i > 0] if max(time_lags) > 0 else [np.nan]),
				max(i for i in time_lags_inv if i > 0)
			])*1.3]
	else:
		time_ylimits	= [
			min([
				min(i for i in time_lags if i > 0),
			])*0.7,
			max([
				max(i for i in time_lags if i > 0),
			])*1.3]

	time_rect = patches.Rectangle(
		( min(fourier_freqs)*0.7
		, time_ylimits[0])
		, (max(fourier_freqs)-min(fourier_freqs))*1.3
		, time_ylimits[1]-time_ylimits[0]
		, linewidth=1, linestyle='--', edgecolor='k', facecolor='none')

	## ----- Compilation: Input -----


	## --- CCF ---
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.0, 0.57, 0.48, 0.10], [])

	ax.plot(model_ccf_lags, model_ccf, color="k")

	ax.set_xlabel('Lag (s)')
	ax.set_ylabel('CCF Coefficient')

	ax.set_xlim(-recovered_ccf_segment/3, recovered_ccf_segment/3)

	ax.grid(linestyle=":", zorder=5)

	ax.axhline(0, linestyle="--", linewidth=1, zorder=9)
	ax.axvline(0, linestyle="--", linewidth=1, zorder=9)



	## --- Power Spectra ---
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.0,  0.42,  0.48, 0.08], [])
	ax.plot(frequencies,  power_spectrum_model_A*frac_rms_norm_A, linewidth=2, linestyle="-", label="Band A", color="blue")
	ax.plot(frequencies,  power_spectrum_model_B*frac_rms_norm_B, linewidth=2, linestyle="-", label="Band B", color="red")

	ax.hlines(0, frequencies[1]/2, max(frequencies)*2, linestyle="--", linewidth=1, zorder=9)

	ax.xaxis.set_ticklabels([])
	ax.xaxis.tick_top()

	ax.set_xscale("log")
	ax.set_yscale("log")

	ax.set_xlim(frequencies[1]/2, max(frequencies)*2)
	ax.set_ylim(ps_ylimits)

	ax.set_xlabel('Frequency (Hz)')
	ax.set_ylabel('F_RMS Power')

	ax.grid(linestyle=":", zorder=5)

	ax.add_patch(ps_rect)


	## --- Coherence ---
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.0, 0.34,  0.48, 0.08], [])
	ax.plot(frequencies,  coherence_model, linestyle="-", linewidth=2, color="darkviolet")
	ax.set_xscale("log")
	ax.set_yscale("log")
	ax.set_ylabel('Coherence')
	ax.grid(linestyle=":", zorder=5)
	ax.set_ylim(coh_ylimits)
	ax.set_xlim(frequencies[1]/2, max(frequencies)*2)

	ax.add_patch(coh_rect)


	## --- Phase & Time Lags ---
	if time_or_phase == 'time':
		model_phase_lags = []
		for j in range(len(model_lags)):
			model_phase_lags.append(model_lags[j] * 2 * np.pi * frequencies[j])


		## --- Phase Lags ---
		##				   left, base, wide, high
		ax = fig.add_axes([ 0.0,  0.26,  0.48, 0.08], [])

		ax.plot(frequencies,  model_phase_lags, linestyle="-", linewidth=2, color="darkviolet")
		tick_pos= np.arange(-2*np.pi, 4*np.pi , np.pi)
		labels = ['-$2\pi$', '-$\pi$', '0', '$\pi$', '$2\pi$', '$3\pi$']
		ax.set_xlim(frequencies[1]/2, max(frequencies)*2)
		ax.set_yticks(tick_pos, labels)
		ax.set_ylabel('Phase Lags (rad)')
		ax.axhline(0, linestyle=":", linewidth=1, zorder=9)
		ax.set_xscale("log")
		ax.set_xlabel("Frequency (Hz)")
		ax.grid(linestyle=":", zorder=5)
		ax.add_patch(phase_rect)


		## --- Time Lags ---
		##				   left, base, wide, high
		ax = fig.add_axes([ 0.0, 0.18,  0.48, 0.08], [])

		ax.plot(frequencies,  model_lags, linestyle="-",  linewidth=2, color="darkviolet")
		ax.plot(frequencies, -model_lags, linestyle="--", linewidth=2, color="darkviolet")
		ax.set_xlim(frequencies[1]/2, max(frequencies)*2)
		ax.set_ylim([min(abs(model_lags))/2, max(np.append(model_lags, -model_lags))*2])
		ax.set_ylabel('Time Lags (s)')
		ax.set_yscale("log")
		ax.set_xscale("log")
		ax.set_xlabel("Frequency (Hz)")
		ax.grid(linestyle=":", zorder=5)
		ax.add_patch(time_rect)

	elif time_or_phase == 'phase':

		## --- Phase Lags ---
		##				   left, base, wide, high
		ax = fig.add_axes([ 0.0,  0.26,  0.48, 0.08], [])

		ax.plot(frequencies, model_lags, linestyle="-", linewidth=2, color="darkviolet")
		ax.set_xlim(frequencies[1]/2, max(frequencies)*2)
		ax.axhline(0, linestyle=":", linewidth=1, zorder=9)

		tick_pos= np.arange(-2*np.pi, 4*np.pi , np.pi)
		labels = ['-$2\pi$', '-$\pi$', '0', '$\pi$', '$2\pi$', '$3\pi$']
		plt.yticks(tick_pos, labels)

		ax.set_xscale("log")
		ax.set_xlabel("Frequency (Hz)")
		ax.set_ylabel('Phase Lag (rad)')
		ax.grid(linestyle=":", zorder=5)
		ax.add_patch(phase_rect)

		## --- Time Lags ---
		##				   left, base, wide, high
		ax = fig.add_axes([ 0.0, 0.18,  0.48, 0.08], [])

		ax.plot(frequencies[1:len(frequencies)],  model_lags[1:len(model_lags)]/(2*np.pi*frequencies[1:len(frequencies)])
			, linestyle="-",  linewidth=2, color="darkviolet")
		ax.plot(frequencies[1:len(frequencies)], -model_lags[1:len(model_lags)]/(2*np.pi*frequencies[1:len(frequencies)])
			, linestyle="--", linewidth=2, color="darkviolet")
		ax.set_xlim(frequencies[1]/2, max(frequencies)*2)
		ax.set_xscale("log")
		ax.set_yscale("log")
		ax.set_xlabel("Frequency (Hz)")
		ax.set_ylabel('Time Lag (s)')
		ax.grid(linestyle=":", zorder=5)
		ax.add_patch(time_rect)


	## ----- Compilation: Output -----


	## --- Correlation Function ---

	##				   left, base, wide, high
	ax = fig.add_axes([ 0.52, 0.57, 0.48, 0.10], [])

	ax.step(ccf_av[:,0], ccf_av[:,1], where='mid', lw=1, color="black")
	ax.set_xlim(-recovered_ccf_segment/3, recovered_ccf_segment/3)

	ax.minorticks_on()
	ax.yaxis.tick_right()
	ax.yaxis.set_label_position("right")
	ax.set_xlabel('Lag (s)')
	ax.set_ylabel('CCF Coefficient')

	ax.grid(linestyle=":", zorder=5)

	ax.axhline(0, linestyle="--", linewidth=1, zorder=9)
	ax.axvline(0, linestyle="--", linewidth=1, zorder=9)

	ax.annotate("CCF Range: " + str(recovered_ccf_segment) + "s", xy=(0.54, 0.55), xycoords="figure fraction")


	## --- Power Spectra ---
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.52,  0.42,  0.48, 0.08], [])
	ax.errorbar(fourier_freqs, powerspectra_A, yerr=powerspectra_A_err \
		, ms=4, lw=0.5, capsize=3, fmt='bo', zorder=9)
	ax.errorbar(fourier_freqs, powerspectra_B, yerr=powerspectra_B_err \
		, ms=4, lw=0.5, capsize=3, fmt='ro', zorder=9)
	ax.set_ylim(ps_ylimits)

	ax.xaxis.set_ticklabels([])
	ax.xaxis.tick_top()

	ax.set_xscale('log')
	ax.set_ylabel('F_RMS Power')
	ax.yaxis.tick_right()
	ax.yaxis.set_label_position("right")
	ax.set_yscale('log', nonpositive='clip')
	ax.grid(linestyle='--')


	## --- Coherence ---
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.52, 0.34,  0.48, 0.08], [])
	ax.set_xscale('log')
	ax.set_yscale('log', nonpositive="clip")
	ax.set_ylim(coh_ylimits)

	ax.errorbar(fourier_freqs, coherence, yerr=coherence_err, lw=0.5, fmt='mo', zorder=9)
	ax.set_ylabel("Coherence")
	ax.yaxis.tick_right()
	ax.yaxis.set_label_position("right")
	ax.grid(linestyle='--')


	## --- Phase Lags ---
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.52,  0.26,  0.48, 0.08], [])
	ax.set_xscale('log')

	ax.yaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
	ax.yaxis.set_minor_locator(plt.MultipleLocator(np.pi / 4))
	ax.yaxis.set_major_formatter(plt.FuncFormatter(corrsim.format_func))
	ax.set_ylim([-3.8, 3.8])

	ax.axhline(0, linestyle=":", linewidth=1, zorder=9)
	ax.errorbar(fourier_freqs, phase_lags, yerr=phase_lags_err, lw=0.5, fmt='mo')

	ax.set_ylabel("Phase Lag (rad)")
	ax.yaxis.tick_right()
	ax.yaxis.set_label_position("right")
	ax.grid(linestyle='--')


	## --- Time Lags ---
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.52, 0.18,  0.48, 0.08], [])
	ax.set_xscale('log')
	ax.set_yscale('log', nonpositive="clip")

	ax.errorbar(fourier_freqs, time_lags,     yerr=time_lags_err, lw=0.5, fmt='mo', label = "+ve Lags")
	ax.errorbar(fourier_freqs, time_lags_inv, yerr=time_lags_err, lw=0.5, linestyle="none", marker='o', fillstyle = "none", color="darkviolet", label = "-ve Lags")

	#ax.legend()

	ax.set_ylabel("Time Lag (s)")
	ax.yaxis.tick_right()
	ax.yaxis.set_label_position("right")
	ax.set_xlabel("Frequency (Hz)")
	ax.grid(linestyle='--')


	## --- Log File ---
	##				   left, base, wide, high
	ax = fig.add_axes([ 0.0, 0.04,  1.0, 0.09], [])
	ax.axis('off')

	ax.text(0.0, 0.8, "Observation Length: " + str(obs_length) + "s")
	ax.text(0.5, 0.8, "Time Resolution: " + str(time_resolution) + "s")
	ax.text(0.0, 0.6, "Mean Counts/s (A): " + str(mean_counts_per_sec_A))
	ax.text(0.5, 0.6, "Mean Counts/s (B): " + str(mean_counts_per_sec_B))
	ax.text(0.0, 0.4, "F_rms (A): " + str(F_rms_A))
	ax.text(0.5, 0.4, "F_rms (B): " + str(F_rms_B))

	ax.text(0.0, 0.1, "Noise Sources:   Red: " + apply_red + "   Poisson: " + apply_poisson + \
		"   Scintillation: " + apply_scintillation + "   Readout: " + apply_readout)


	## ----- Save Figure----

	savename = "./" + output_dir + "/" + fileprefix + "Compilation.png"
	plt.savefig(savename, bbox_inches='tight')

	plt.close('all')


# os.system('say "Slep fox."')
