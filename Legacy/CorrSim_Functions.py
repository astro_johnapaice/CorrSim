## A selection of functions for use with CorrSim.

import math
import cmath
import numpy as np
import scipy.signal as ss
from stingray import Lightcurve, AveragedPowerspectrum, AveragedCrossspectrum
import time


##	----------------------------------
##			General Functions
##	----------------------------------

def largest_prime_factor(n):			## Finds the largest prime factor of a number n
    i = 2
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
    return n

def place(value, list):					## Finds the (first) index of a given value in a list, or the closest to it.
	return min(range(len(list)), key=lambda i: abs(list[i]-value))

def ma(x, N):							## Creates a Moving Average of a list x, over N points.
	cumsum = np.cumsum(np.insert(x, 0, 0))
	return (cumsum[N:] - cumsum[:-N]) / float(N)

def rev(lst):							## Reverses a list
	return [ele for ele in reversed(lst)]

def ccf(x, y, lag=10000):				## Creates an R-like Cross Correlation Function of two series (x, y)
	result = ss.correlate(y - np.mean(y), x - np.mean(x), method='direct') \
		/ (np.std(y) * np.std(x) * len(y))
	length = (len(result) - 1) // 2
	lo = length - lag
	hi = length + (lag + 1)

	if lo < 0:		## If the maximum lag goes negative, then reset to zero for intended behaviour.
		lo = 0

	return result[lo:hi]

def split_list(list, n):				## Splits a list into segments of n length
										## Usage: list(split_list(list,n))
	for i in range(0, len(list), n):
		yield list[i:(i + n)]

def format_func(value, tick_number):	## For plotting phases in units of pi/2
	N = int(np.round(2 * value / np.pi))
	if N == -2:
		return r"-$\pi$"
	elif N == -1:
		return r"-$\pi/2$"
	elif N == 0:
		return "0"
	elif N == 1:
		return r"$\pi/2$"
	elif N == 2:
		return r"$\pi$"
	elif N % 2 > 0:
		return r"${0}\pi/2$".format(N)
	else:
		return r"${0}\pi$".format(N // 2)

def statAv(rate,n):						## Binning a series every n points

	binning_half	= int(np.floor(n/2))

	rate_binned		= []
	k				= binning_half + 1
	for i in range(int(np.floor(len(rate)/n))):
		while k < len(rate):
			rate_binned.append(np.mean(rate[(k - binning_half):(k + binning_half)]))
			k += n

	return rate_binned


def sort_list(list1, list2):			## Sort list1 using an ordered list2
	zipped_pairs = zip(list2, list1)
	z = [x for _, x in sorted(zipped_pairs)]
	return z


##	------------------------------------------
##			Log Distribution Functions
##	------------------------------------------
## Find the given distributions in either log-log or semi-log space, from given coordinates.

def powerlaw(x1, x2, y1, y2):										## --- Powerlaw
	slope	= math.log(y1/y2, x1/x2)								## Find the power that connects the two points,
	norm	= y1/(x1**slope)										## and the normalisation.
	return [norm, slope]

def linear(x_1, y_1, x_2, y_2):										## --- Linear
	slope		= (y_2-y_1) / (x_2-x_1)								## Find the slope between the two points,
	intercept	= y_2 - slope * x_2									## and the intercept with the y axis.
	return [slope, intercept]

def semilog_linear(x_1, y_1, x_2, y_2, n):							## --- Linear (in semi-log space)
	slope		= (y_2-y_1) / ( math.log(x_2,n)-math.log(x_1,n) )	## Find the slope between the two points,
	intercept	= y_2 - slope*math.log(x_2,n)						## and the intercept with the y axis.
	return [slope, intercept]

def log_linear(x_1, y_1, x_2, y_2, n):								## --- Linear (in log space)
	slope		= ( math.log(y_2,n)-math.log(y_1,n) ) / ( math.log(x_2,n)-math.log(x_1,n) )		## Find the slope between the two points,
	intercept	= math.log(y_2,n) - slope*math.log(x_2,n)			## and the intercept with the y axis.
	return [slope, intercept]

def semilog_polynomial(x_1, y_1, x_2, y_2, x_3, y_3, n):			## --- Polynomial (in semi-log space)
	a	= [math.log(x_1,n), math.log(x_2,n), math.log(x_3,n)]		## Find the three x-axis values...
	b	= [y_1, y_2, y_3]											## Set the three y-axis values...
	solved_poly = np.polyfit(a,b,2)									## And solve the polynomial for them.
	return solved_poly

def log_polynomial(x_1, y_1, x_2, y_2, x_3, y_3, n):				## --- Polynomial (in log space)
	a	= [math.log(x_1,n), math.log(x_2,n), math.log(x_3,n)]		## Find the three x-axis values...
	b	= [math.log(y_1,n), math.log(y_2,n), math.log(y_3,n)]		## Set the three y-axis values...
	solved_poly = np.polyfit(a,b,2)									## And solve the polynomial for them.
	return solved_poly

## Lag Distribution function for time lags: Return the required distribution in the form Ax**2 + Bx**P + C
def lag_section_time(distribution, start_freq, close_freq, start_lag, close_lag=0, extra_freq=0, extra_lag=0):

	A = 0
	B = 0
	C = 0
	P = 1
	start = start_freq
	close = close_freq
	log = 0

	if distribution == "Constant":

		C = start_lag

	elif distribution == "Linear":
		phase_params = powerlaw(start_freq, close_freq, start_lag, close_lag)

		B = phase_params[0]
		P = phase_params[1]

	elif distribution == "Polynomial":

		phase_params = log_polynomial(
			start_freq, start_lag,
			extra_freq, extra_lag,
			close_freq, close_lag,
			10
		)

		A = phase_params[0]
		B = phase_params[1]
		C = phase_params[2]
		log = 1

	elif distribution == "Power":

		phase_params = linear(start_freq, start_lag, close_freq, close_lag)

		B = phase_params[0]
		C = phase_params[1]

	elif distribution == "Constant_Phase":

		B = start_lag/(2*np.pi)
		P = -1

	return [start, close, A, B, C, P, log]


## Lag Distribution function for phase lags: Return the required distribution in the form Ax**2 + Bx**P + C
def lag_section_phase(distribution, start_freq, close_freq, start_lag, close_lag=0, extra_freq=0, extra_lag=0):

	A = 0
	B = 0
	C = 0
	P = 1
	start = start_freq
	close = close_freq
	log = 0

	if distribution == "Constant":

		C = start_lag

	elif distribution == "Linear":

		phase_params = semilog_linear(start_freq, start_lag, close_freq, close_lag, 10)

		B = phase_params[0]
		C = phase_params[1]
		log = 1

	elif distribution == "Polynomial":

		phase_params = semilog_polynomial(
			start_freq, start_lag,
			extra_freq, extra_lag,
			close_freq, close_lag,
			10
		)

		A = phase_params[0]
		B = phase_params[1]
		C = phase_params[2]
		log = 1

	elif distribution == "Power":

		phase_params = linear(start_freq, start_lag, close_freq, close_lag)

		B = phase_params[0]
		C = phase_params[1]

	elif distribution == 'Constant_Time':

		B = start_lag*2*np.pi

	return [start, close, A, B, C, P, log]



##	----------------------------------
##			Main Functions
##	----------------------------------


##			Create Power Spectra
##	----------------------------------

## Creates power spectra using broken powerlaws
def power_model_broken_powerlaw(frequencies, ps_power, break_freq, coh_constant, coh_power, coh_break_freq):

	coherence		= np.zeros(len(frequencies)) + coh_constant			## Define constant section of Coherence
	coh_break_point = place(coh_break_freq, frequencies)				## Find the break point for the Coherence
	coherence[coh_break_point:len(coherence)] =	\
		(frequencies[coh_break_point:len(coherence)] ** coh_power) * \
		(coh_constant / (frequencies[coh_break_point] ** coh_power))	## Define and normalise the power law section of the Coherence

	unnorm_power_spectrum_A		= np.append(0, (frequencies[1:len((frequencies)+1)] ** ps_power))					## Define Band A PS...
	unnorm_power_spectrum_B_coh	= np.append(0, (frequencies[1:len((frequencies)+1)] ** ps_power)) * coherence		## Define Coherent Band B PS...
	unnorm_power_spectrum_B_inc	= np.append(0, (frequencies[1:len((frequencies)+1)] ** ps_power)) * (1 - coherence)	## Define Incoherent Band B PS...

	break_point = place(break_freq, frequencies)						## Define the break point for the power spectra
	constant	= frequencies[break_point] ** ps_power					## Find the value for the constant portion of the power spectrum

	unnorm_power_spectrum_A[    1:break_point]	= constant				## Set the values below the break point to the constant (Band A)
	unnorm_power_spectrum_B_coh[1:break_point]	= (coherence[1:break_point]*0 + constant) * coherence[1:break_point]		## Set the values below the break point as constant (based on coherence)
	unnorm_power_spectrum_B_inc[1:break_point]	= (coherence[1:break_point]*0 + constant) * (1 - coherence[1:break_point])	## Set the values below the break point as constant (based on incoherence)

	return [unnorm_power_spectrum_A, unnorm_power_spectrum_B_coh, unnorm_power_spectrum_B_inc, coherence]

## Creates power spectra using lorentzians
def power_model_lorentzians(frequencies, Lorentz_params_A, Lorentz_params_B):

	## ========== Series A ==========

	num_lorentz_A = int(len(Lorentz_params_A)/3)																## Find number of Lorentzians
	Lorentz_params_array_A = np.transpose(np.array(Lorentz_params_A).reshape(num_lorentz_A, 3))					## Read in the parameters
	Lorentzians_A = np.zeros((len(frequencies), num_lorentz_A))													## Create an array for the Lorentzians

	for i in range(num_lorentz_A):																				## For each Lorentzian...
		Lorentzians_A[:,i]	= Lorentz_params_array_A[0,i] * (1/np.pi) * (0.5 * Lorentz_params_array_A[1,i]) / \
			((frequencies - Lorentz_params_array_A[2,i])**2 + (0.5 * Lorentz_params_array_A[1,i])**2)			## Create it as a function of frequency

	unnorm_power_spectrum_A = np.zeros(len(frequencies))														## Set up the power spectrum...
	for i in range(1, len(frequencies)):
		unnorm_power_spectrum_A[i] = sum(Lorentzians_A[i,:])													## And sum all the Lorentzians to make it.


	## ========== Series B ==========

	num_lorentz_B = int(len(Lorentz_params_B)/4)													## Find number of Lorentzians

	Lorentz_params_array_B = np.transpose(np.array(Lorentz_params_B).reshape(num_lorentz_B, 4))		## Read in the parameters

	Lorentzians_B_coh = np.zeros((len(frequencies), num_lorentz_B))									## Create an array for the coherent Lorentzians
	Lorentzians_B_inc = np.zeros((len(frequencies), num_lorentz_B))									## Create an array for the incoherent Lorentzians

	for i in range(num_lorentz_B):																	## For each Lorentzian...
		Lorentzians_B_coh[:,i]	= ( Lorentz_params_array_B[0,i] * (1/np.pi) * (0.5 * Lorentz_params_array_B[1,i]) / \
			((frequencies - Lorentz_params_array_B[2,i])**2 + (0.5 * Lorentz_params_array_B[1,i])**2) ) * Lorentz_params_array_B[3,i]		## Create the coherent part...
		Lorentzians_B_inc[:,i]	= ( Lorentz_params_array_B[0,i] * (1/np.pi) * (0.5 * Lorentz_params_array_B[1,i]) / \
			((frequencies - Lorentz_params_array_B[2,i])**2 + (0.5 * Lorentz_params_array_B[1,i])**2) ) * (1-Lorentz_params_array_B[3,i])	## ...and the incoherent part.

	unnorm_power_spectrum_B_coh	= np.zeros(len(frequencies))										## Set up the coherent power spectrum,
	unnorm_power_spectrum_B_inc	= np.zeros(len(frequencies))										## and the incoherent one,
	coherence_model				= np.zeros(len(frequencies))										## as well as the model coherence.
	for i in range(1, len(frequencies)):
		unnorm_power_spectrum_B_coh[i]	= sum(Lorentzians_B_coh[i,:])								## Sum the coherent lorentzians,
		unnorm_power_spectrum_B_inc[i]	= sum(Lorentzians_B_inc[i,:])								## And the incoherent lorentzians
		coherence_model[i]				= sum(Lorentzians_B_coh[i,:])/sum(Lorentzians_B_inc[i,:])	## And divide for the coherence.

	return [unnorm_power_spectrum_A, unnorm_power_spectrum_B_coh, unnorm_power_spectrum_B_inc, \
		coherence_model, Lorentzians_A, Lorentzians_B_coh, Lorentzians_B_inc, num_lorentz_A, num_lorentz_B]



##		Normalise Power Spectra
##	----------------------------------

## Normalises power spectra; Uses F_rms normalisation.
def normalise_power_spectra(unnorm_power_spectrum_A, F_rms_A, unnorm_power_spectrum_B_coh, \
	unnorm_power_spectrum_B_inc, F_rms_B, obs_length, time_res, num_bins, \
	mean_counts_A, mean_counts_B):

	## ========== Series A ==========

	normalisation_A			= ( F_rms_A**2 / sum(unnorm_power_spectrum_A / obs_length) )	## Define normalisation
	power_spectrum_A		= unnorm_power_spectrum_A * normalisation_A						## Apply normalisation

	frac_rms_norm_A			= ((2 * time_res) / (num_bins * mean_counts_A**2))				## Find F_rms normalisation (Vaughan 2003, Pg. 12)
	power_spectrum_A		= power_spectrum_A / frac_rms_norm_A							## And apply that too.


	## ========== Series B ==========

	normalisation_B			= ( F_rms_B**2 /												## Define normalisation
								sum((unnorm_power_spectrum_B_coh + unnorm_power_spectrum_B_inc)
									 / obs_length)
								)
	power_spectrum_B_coh	= ( unnorm_power_spectrum_B_coh ) * normalisation_B				## Apply normalisation to coherent power spectrum
	power_spectrum_B_inc	= ( unnorm_power_spectrum_B_inc ) * normalisation_B				## and incoherent power spectrum

	frac_rms_norm_B			= ((2 * time_res) / (num_bins * mean_counts_B**2))				## Find F_rms normalisation (Vaughan 2003, Pg. 12)
	power_spectrum_B_coh	= power_spectrum_B_coh / frac_rms_norm_B						## And apply that to the coherent...
	power_spectrum_B_inc	= power_spectrum_B_inc / frac_rms_norm_B						## ...and incoherent power spectra too.

	power_spectrum_B		= power_spectrum_B_coh + power_spectrum_B_inc					## Combine to make the final power spectrum for Series B.

	return [power_spectrum_A, power_spectrum_B, power_spectrum_B_coh, power_spectrum_B_inc, \
		normalisation_A, frac_rms_norm_A, normalisation_B, frac_rms_norm_B]



## 		Apply Red Noise
##	------------------------

## Add red noise to a power spectrum.
def red_noise(frequencies, power_spectrum, mean_counts, num_bins):
	'''
	Timmer & Koenig 1995:

	Start with your chosen power spectrum

	For each Fourier Frequency, draw two Gaussian distributed random numbers.
	Multiply these numbers by np.sqrt(0.5*(power_spectrum[i]))
	Result is the real and imaginary part of the Fourier transformed data

	If even number of points, the Nyquist freq is always real. Thus only one number must be drawn.

	To obtain a real-valued time series, choose the Fourier components for the negative frequencies
	according to f(-w[i]) = f*(w[i])

	Obtain the time series by backward Fourier transformation of f(w) from the freq domain
	to the time domain
	'''

	rednoise_1 = np.random.normal(0, size=len(power_spectrum))							## First random number
	rednoise_2 = np.random.normal(0, size=len(power_spectrum))							## Second random number

	for x in range(len(frequencies)):													## For each frequency...
		rednoise_1[x] = np.sqrt(0.5 * power_spectrum[x])*rednoise_1[x]					## Multiply for the first,
		rednoise_2[x] = np.sqrt(0.5 * power_spectrum[x])*rednoise_2[x]					## And for the second.

	FTL_real		= np.append(rednoise_1, rev( rednoise_1[1:(len(rednoise_1)-1)]))	## The first number is the real part of the Fourier-transformed data...
	FTL_imag		= np.append(rednoise_2, rev(-rednoise_2[1:(len(rednoise_2)-1)]))	## and the second number is the imaginary part.

	FTL_imag[0]	= 0																		## Set the first imaginary bin as zero,
	if len(frequencies) % 2 == 1:														## And if it's an odd number of frequencies,
		FTL_imag[len(frequencies)-1]	= 0												## the last freq. bin too.

	amplitude		= abs(FTL_real + FTL_imag*1j)										## Find the amplitude by taking the absolute values,
	amplitude[0]	= mean_counts*num_bins												## and setting the first bin with the mean count rate.

	power_spectrum = amplitude[0:(len(frequencies))]**2									## The power spectrum is the square of the amplitude!

	return power_spectrum


##		  Create Amplitudes and Arguments for each Power Spectra
##	--------------------------------------------------------------------

## First step in converting the power spectrum to lightcurves!
def amplitudes_and_arguments(power_spectrum_A, power_spectrum_B_coh, power_spectrum_B_inc, \
	mean_counts_A, mean_counts_B, num_bins, frequencies, model_lag_array, overall_lag):
	##
	## ===== Convert Power Spectrum to Amplitude + Arguments =====
	##

	## Find the amplitude by taking the square root of of the powers, remembering to reverse the list for the second half.
	amplitude_A					= np.sqrt(np.append(power_spectrum_A,		rev(power_spectrum_A[    1:(len(power_spectrum_A	)-1)])))
	amplitude_B_coh				= np.sqrt(np.append(power_spectrum_B_coh, 	rev(power_spectrum_B_coh[1:(len(power_spectrum_B_coh)-1)])))
	amplitude_B_inc				= np.sqrt(np.append(power_spectrum_B_inc, 	rev(power_spectrum_B_inc[1:(len(power_spectrum_B_inc)-1)])))

	## Define the first bin using the mean count rate (but only do this once for Series B)
	amplitude_A[0]				= mean_counts_A*num_bins
	amplitude_B_coh[0]			= mean_counts_B*num_bins
	amplitude_B_inc[0]			= 0

	## Define the arguments
	arguments_A					= np.random.uniform(low=-np.pi, high=np.pi, size = len(frequencies))					## Randomly generate
	arguments_A					= np.append(arguments_A, rev(np.ndarray.tolist(-arguments_A[1:(len(arguments_A)-1)])))	## Add and reverse for 2nd half
	arguments_A[0]				= 0																						## And remember; first bin = 0

	if num_bins % 2 == 1:						## If we have an odd number of bins,
		arguments_A[int(num_bins/2)]	= 0		## The middle on is zero.


	##
	## ===== 2nd Power Spectrum: Create second set of Arguments =====
	##

	## This will be calculating the lags from the lag parameters you set earlier.
	model_lags = np.zeros(len(frequencies)) + overall_lag			## Define the basic model lags

	## We'll be considering each section defined in turn.
	section_pointer = 0												## Create a variable that points to a section
	for i in range(len(model_lags)):								## For each frequency bin...
		## First, get into a defined section.
		if frequencies[i] < model_lag_array[section_pointer, 0]:	## If we're not yet in the current section,
			next													## Continue until we reach it.
		elif frequencies[i] > model_lag_array[section_pointer, 1]:	## If we're past the current section.
			section_pointer = section_pointer + 1					## Increment to the next section
		if section_pointer == len(model_lag_array[:,1])-1:			## If we're out of defined sections.
			break													## Break.

		elif frequencies[i] >= model_lag_array[section_pointer, 0] and frequencies[i] <= model_lag_array[section_pointer, 1]:	## If we're in a section,
			if (model_lag_array[section_pointer, 6] == 1):																		## If we need to use logs,
				model_lags[i] = model_lag_array[section_pointer, 2]*math.log(frequencies[i],10)**2 +	\
					model_lag_array[section_pointer, 3]*math.log(frequencies[i],10)**model_lag_array[section_pointer, 5] + \
					model_lag_array[section_pointer, 4]																			## Calculate the lag based on the section's parameters.
			else:																												## If we don't need to use logs,
				model_lags[i] = model_lag_array[section_pointer, 2]*frequencies[i]**2 + \
					model_lag_array[section_pointer, 3]*frequencies[i]**model_lag_array[section_pointer, 5] + \
					model_lag_array[section_pointer, 4]																			## Calculate the lag based on the section's parameters.

	## Calculate and set the arguments now...
	arguments_B_coh 					= arguments_A				## The coherent part is initially the same as Series A...
	arguments_B_coh 					= arguments_B_coh - np.append(model_lags, rev(-model_lags[1:(len(model_lags)-1)]))	## Then all frequencies are delayed by a lag dependant on a distribution
	arguments_B_coh[0]					= 0							## (But not the first one)

	## The incoherent arguments are random, however.
	arguments_B_inc						= np.random.uniform(low=-np.pi, high=np.pi, size=len(frequencies))
	arguments_B_inc						= np.append(arguments_B_inc, rev(np.ndarray.tolist(-arguments_B_inc[1:(len(arguments_B_inc)-1)])))
	arguments_B_inc[int(num_bins/2+1)]	= 0
	arguments_B_inc[0]					= 0							## (Still don't forget the first one!)


	return [amplitude_A, amplitude_B_coh, amplitude_B_inc, arguments_A, arguments_B_coh, arguments_B_inc, model_lags]



# ## 				TEST: Calculate Model CCF
# ##	------------------------------------------------
#
# def calc_model_ccf2(amplitude_A, amplitude_B_coh, amplitude_B_inc, arguments_A, arguments_B_coh, \
# 	arguments_B_inc, model_lags):#, power_spectrum_model_A, power_spectrum_model_B):
#
# 	#arguments_B_coh		= arguments_B_coh - arguments_A
# 	arguments_B_coh		= np.append(model_lags, rev(-model_lags[1:(len(model_lags)-1)]))
# 	arguments_B_inc		= arguments_B_inc*0
# 	arguments_A			= arguments_A*0
#
# 	FTL_A				= amplitude_A * np.cos(arguments_A) + 1j * amplitude_A * np.sin(arguments_A)
#
# 	FTL_B_conj_coh		= amplitude_B_coh * np.cos(arguments_B_coh) - 1j * amplitude_B_coh * np.sin(arguments_B_coh)
# 	FTL_B_conj_inc		= amplitude_B_inc * np.cos(arguments_B_inc) - 1j * amplitude_B_inc * np.sin(arguments_B_inc)
#
# 	FTL_B_conj_total	= rev(FTL_B_conj_coh + FTL_B_conj_inc)
#
# 	coherence_model		= np.fft.ifft(FTL_A * FTL_B_conj_total)
#
# 	return coherence_model


##      Create Model CCF
##	----------------------------------

def calc_model_CCF(coherence_model, power_spectrum_model_A, frac_rms_norm_A, \
	power_spectrum_model_B, frac_rms_norm_B, model_lags, time_resolution, F_rms_A, F_rms_B):

	## We first make amplitudes and arguments...
	amp_1			= coherence_model * np.sqrt(power_spectrum_model_B*frac_rms_norm_B * power_spectrum_model_A*frac_rms_norm_A)
	amplitude_ccf	= np.sqrt(np.append(amp_1, rev(amp_1[1:(len(amp_1)-1)])))
	arguments_ccf	= np.append(model_lags, rev(-model_lags[1:(len(model_lags)-1)]))

	if len(arguments_ccf) % 2 == 1:
		arguments_ccf[int(arguments_ccf/2)]	= 0

	FT_ccf		= amplitude_ccf * np.cos(arguments_ccf) + 1j * amplitude_ccf * np.sin(arguments_ccf)	## Find the Fourier Transformed version,
	IFT_ccf		= np.fft.ifft(FT_ccf)																	## Then inversely transform it...

	ccf_normalisation	= 1 / (2 * time_resolution * np.sqrt(F_rms_A * F_rms_B))						## Find the normalisation,

	model_ccf	= IFT_ccf.real * ccf_normalisation														## And calculate it!

	## It's initially formatted wrongly about the middle - so cut in two, and stitch together
	model_ccf_former = model_ccf[0:int(len(model_ccf)/2)]
	model_ccf_latter = model_ccf[int(len(model_ccf)/2):len(model_ccf)]

	model_ccf = rev(np.append(model_ccf_latter, model_ccf_former))

	## Calculate the lag!
	lag = np.arange(1, len(model_ccf)+1)
	lag = lag - int(len(model_ccf)/2)
	lag = lag*time_resolution

	return [lag, model_ccf]



## 		 Convert Power Spectra to Lightcurve
##	------------------------------------------------

def ps_to_lightcurve(amplitude_A, amplitude_B_coh, amplitude_B_inc, arguments_A, arguments_B_coh, \
	arguments_B_inc):

	## Create a Fourier-transformed lightcurve
	FTL_A		= amplitude_A * np.cos(arguments_A) + 1j * amplitude_A * np.sin(arguments_A)	## Combine amplitude and arguments
	LC_A		= np.fft.ifft(FTL_A)															## Inversely transform
	flux_A		= LC_A.real																		## Find the flux for Series A

	## Do the same for Series B
	FTL_B_coh	= amplitude_B_coh * np.cos(arguments_B_coh) + 1j * amplitude_B_coh * np.sin(arguments_B_coh)	## Combine amplitude and arguments...
	FTL_B_inc	= amplitude_B_inc * np.cos(arguments_B_inc) + 1j * amplitude_B_inc * np.sin(arguments_B_inc)	## Combine amplitude and arguments...

	FTL_B_total	= FTL_B_coh + FTL_B_inc		## And at long last, combine them together!
	LC_B_total	= np.fft.ifft(FTL_B_total)	## Inversely transform,
	flux_B		= LC_B_total.real			## And hey presto - the Series B lightcurve!

	## Correct negative counts - assume nothing in that bin.
	flux_A[flux_A<0] = 0
	flux_B[flux_B<0] = 0

	return [flux_A, flux_B]



## 			Apply Noise
##	---------------------------
## Various noise sources

def apply_poisson_noise(flux):
	return np.random.poisson(flux, len(flux))

def apply_scintillation_noise(scin_noise, flux):
	return np.random.normal(loc = flux, scale = np.sqrt(scin_noise)*flux, size = len(flux))

def apply_readout_noise(readout_noise, flux):
	return flux + np.random.normal(loc = 0, scale = readout_noise, size = len(flux))


## 			 Convert Lightcurve to Power Spectrum
##	------------------------------------------------------

def power_spectrum_recovery(flux_A, flux_B, num_bins, obs_length, frac_rms_norm_A, frac_rms_norm_B):

	flux_A_zeromean	= flux_A - np.mean(flux_A)			## Subtract off the mean flux
	flux_B_zeromean	= flux_B - np.mean(flux_B)			## Subtract off the mean flux
	freqs_A			= abs(np.fft.fft(flux_A_zeromean))	## Apply FFT
	freqs_B			= abs(np.fft.fft(flux_B_zeromean))	## Apply FFT

	#Create a frequency spectrum
	freqs_A[1:len(freqs_A)]	= frac_rms_norm_A * freqs_A[1:len(freqs_A)] ** 2	## Series A
	freqs_B[1:len(freqs_B)]	= frac_rms_norm_B * freqs_B[1:len(freqs_B)] ** 2	## Series B

	#Save the Freqs data
	freqsarr		= np.zeros((int(num_bins/2),3))			## Set it up first...
	freqsarr[:,0]	= range(1,(int(num_bins/2)+1))			## Frequencies
	freqsarr[:,0]	= freqsarr[:,0]/(obs_length)			## Frequencies (pt. 2)
	freqsarr[:,1]	= freqs_A[0:(int(len(freqs_A)/2))]		## Series A
	freqsarr[:,2]	= freqs_B[0:(int(len(freqs_B)/2))]		## Series B

	return freqsarr

## This next function is the same as red_noise(), but only returns the amplitude.
def apply_red_noise_av_ps(frequencies, power_spectrum):

	rednoise_1 = np.random.normal(0, size=len(power_spectrum))
	rednoise_2 = np.random.normal(0, size=len(power_spectrum))

	for x in range(len(frequencies)):
		rednoise_1[x] = np.sqrt(0.5 * power_spectrum[x])*rednoise_1[x]
		rednoise_2[x] = np.sqrt(0.5 * power_spectrum[x])*rednoise_2[x]

	FTL_real		= np.append(rednoise_1, rev( rednoise_1[1:(len(rednoise_1)-1)]))
	FTL_imag		= np.append(rednoise_2, rev(-rednoise_2[1:(len(rednoise_2)-1)]))

	FTL_imag[0]	= 0
	if len(frequencies) % 2 == 1:
		FTL_imag[len(frequencies)-1]	= 0

	amplitude		= abs(FTL_real + FTL_imag*1j)

	return amplitude

## Recover the power spectrum by averaging over many power spectra
## This can be to check what infinitely(ish) many power spectra would look like (in case you want to check against the model)
def averaged_power_spectrum_recovery(frequencies, mean_counts, num_bins, power_spectrum_model, frac_rms_norm, average_ps_iterations, \
	apply_red, apply_poisson, apply_scintillation, apply_readout, scin_noise, read_noise):
	## ===== Averaged method: Convert to many lightcurves, make a power spectrum from each, and average =====

	lightcurve_array	= np.zeros((num_bins,average_ps_iterations))
	powers_array		= np.zeros((num_bins,average_ps_iterations))
	freqsarr_array		= np.zeros((int(num_bins/2)+1,average_ps_iterations))

	start_seconds = time.time()
	for i in range(average_ps_iterations):

		print("-> ", round(time.time() - start_seconds), "s elapsed, ",
			round((i+1)/average_ps_iterations*100), "% Complete (", (i+1), " iter.). ~",
			round((time.time() - start_seconds) * ((1-((i+1)/average_ps_iterations)) /
			((i+1)/average_ps_iterations))), "s remaining. <-  ", sep="", end="\r")

		## --- From the amplitude, create a lightcurve... ---
		## Create arguments...
		arguments_averaged						= np.random.uniform(low=-np.pi, high=np.pi, size=len(frequencies))
		arguments_averaged						= np.append(arguments_averaged, rev(np.ndarray.tolist(-arguments_averaged[1:(len(arguments_averaged)-1)])))
		arguments_averaged[0]					= 0
		if len(frequencies) % 2 == 0:
			arguments_averaged[int(num_bins/2)]	= 0

		if apply_red == "A" or apply_red == "AB":
			amplitude = apply_red_noise_av_ps(frequencies, power_spectrum_model)
			amplitude[0]	= mean_counts*num_bins

		## Inverse fourier transform....
		FTL										= amplitude * np.cos(arguments_averaged) + 1j * amplitude * np.sin(arguments_averaged)
		LC										= np.fft.ifft(FTL)
		lightcurve_array[:,i]					= LC.real

		lightcurve_array[lightcurve_array[:,i]<0,i] = 0

		#Add noise, if used...
		if apply_poisson == "A" or apply_poisson == "AB":
			lightcurve_array[:,i] = apply_poisson_noise(lightcurve_array[:,i])

		if apply_scintillation == "A" or apply_scintillation == "AB":
			lightcurve_array[:,i] = apply_scintillation_noise(scin_noise, lightcurve_array[:,i])

		if apply_readout == "A" or apply_readout == "AB":
			lightcurve_array[:,i] = apply_readout_noise(read_noise, lightcurve_array[:,i])

		lightcurve_array[lightcurve_array[:,i]<0,i] = 0

		## --- From the Lightcurve, create a power spectrum... ---
		LC_temp									= lightcurve_array[:,i]-np.mean(lightcurve_array[:,i])			## Remove Mean
		powers_array[:,i]						= abs(np.fft.fft(LC_temp))										## Apply FFT
		powers_array[1:len(powers_array),i]		= frac_rms_norm * powers_array[1:len(powers_array),i] ** 2	## Create freq spectrum
		freqsarr_array[:,i]						= powers_array[0:(int(num_bins/2)+1),i]							## Collate data


	print("-> ", round(time.time() - start_seconds), "s elapsed, 100% Complete (", average_ps_iterations, " iter.). <-               ", sep="")

	freqsav				= np.zeros((int(num_bins/2),3))
	freqsav[:,0]		= frequencies[1:len(frequencies)]

	for i in range(int(num_bins/2)):
		freqsav[i,1] 	= np.mean(freqsarr_array[i,:])
		freqsav[i,2] 	= np.sqrt((np.var(freqsarr_array[i,]))/average_ps_iterations)

	return freqsav, freqsarr_array


## 			Calculate Average CCF
##	----------------------------------------

def recovered_ccf(rateA, rateB, time_res, seconds, binning):

	##			Binning (Optional)
	## For if the data is high resolution, and you just want a quick look...
	if binning > 1:
		rateA_used		= statAv(rateA,binning)
		rateB_used		= statAv(rateB,binning)
		time_res_used	= time_res * binning
		print("Binning Complete.")

	else:
		rateA_used		= rateA
		rateB_used		= rateB
		time_res_used	= time_res


	##			Segment Calculation
	## Find the number of bins per segment
	bins_per_segment = int(np.floor(seconds/time_res_used))

	## Check to make sure that the segment sizes are big enough
	if (bins_per_segment <= 10):
		print("WARNING: 10 or less points per segment in one band. Reduce binning or increase segment size.")
		#quit()

	## Split up the data into segments of that size
	rateA_segments = list(split_list(rateA_used, bins_per_segment))
	rateB_segments = list(split_list(rateB_used, bins_per_segment))

	## If the last segment does not have the right size, then discard
	if len(rateA_segments[0]) != len(rateA_segments[-1]):
		rateA_segments = rateA_segments[0:-1]
		rateB_segments = rateB_segments[0:-1]

	num_segments = len(rateA_segments)


	##		Begin CCF of each section...
	ccf_arr = np.zeros(((bins_per_segment*2)-1,num_segments))
	for i in range(num_segments):

		## From the full time range, select the data for this segment.
		ra = rateA_segments[i]
		rb = rateB_segments[i]

		## Prewhiten the data - i.e. remove any long-term trends.
		pre_whiten = "(Pre-Whitened)"
		#pre_whiten = "n"
		if (pre_whiten == "(Pre-Whitened)"):
			av_range = 10
			rateA_start = np.mean(ra[0:av_range])
			rateA_close = np.mean(ra[(len(ra)-(av_range-1)):len(ra)])
			rateB_start = np.mean(rb[0:av_range])
			rateB_close = np.mean(rb[(len(rb)-(av_range-1)):len(rb)])

			ra2 = []
			rb2 = []
			fluxDiffA	= rateA_start-rateA_close
			fluxDiffB	= rateB_start-rateB_close
			for k in range(bins_per_segment):
				fraction	= (k)/(bins_per_segment)
				ra2.append(rateA_start-fluxDiffA*fraction)
				rb2.append(rateB_start-fluxDiffB*fraction)

			ra = (ra - ra2) + np.mean(ra)
			rb = (rb - rb2) + np.mean(rb)

		## Calculate the CCF!
		ccf_arr[:,i] = ccf(ra, rb, bins_per_segment)

	##	 Post-Production - Average the results
	ccf_av = np.zeros(((bins_per_segment*2)+1,3))
	ccf_av[:,0] = np.arange(-bins_per_segment, bins_per_segment+1)*time_res_used
	for i in range((bins_per_segment*2)-1):
		ccf_av[i,1] = np.nanmean(ccf_arr[i,:])
		ccf_av[i,2] = np.nanstd( ccf_arr[i,:]) / np.sqrt(num_segments)

	return ccf_av


## 			Calculate Fourier Data
##	--------------------------------------

def fourier(time, band_1, band_2, time_resolution, segment_size, rebin, reference_freq, apply_poisson, remove_whitenoise):

	## --- Create Lightcurves ---
	lightcurve_A = Lightcurve(time, band_1, dt=time_resolution, skip_checks=True)
	lightcurve_B = Lightcurve(time, band_2, dt=time_resolution, skip_checks=True)


	## --- Create Power Spectra ---
	avg_ps_A = AveragedPowerspectrum(lightcurve_A, segment_size, norm='frac')
	avg_ps_B = AveragedPowerspectrum(lightcurve_B, segment_size, norm='frac')


	## --- Binning & White Noise ---
	powerspectra_binned_A = avg_ps_A.rebin_log(f=rebin)
	powerspectra_binned_B = avg_ps_B.rebin_log(f=rebin)

	if remove_whitenoise == "y":

		## Calculate white/poisson noise.
		## This assumes that the last few points are white-noise dominated.
		## The last point is usually based on only one bin, so we calculate it
		##	from the second-to-last point and assume the white noise is99% of that.
		## Note that this is a big approximation - proper fitting should be done for an actual calculation!

		white_noise_A = np.mean(powerspectra_binned_A.power[-2])*0.99
		white_noise_B = np.mean(powerspectra_binned_B.power[-2])*0.99

	else:
		white_noise_A = 0
		white_noise_B = 0

	## Remove the white noise (or not, if it's set to zero)
	powereadout_noiseless_binned_A = powerspectra_binned_A.power - white_noise_A
	powereadout_noiseless_binned_B = powerspectra_binned_B.power - white_noise_B

	## --- Cross Spectra ---
	cross_spectrum = AveragedCrossspectrum(lightcurve_A, lightcurve_B, segment_size, norm='none')
	cross_spectrum_binned = cross_spectrum.rebin_log(f=rebin)

	## --- Coherence ---
	coherence, err_coherence = cross_spectrum.coherence()						## Original
	coherence_binned, err_coherence_binned = cross_spectrum_binned.coherence()	## Binned

	#coherence_intrinsic_binned	= (coherence_binned * np.sqrt(powerspectra_binned_B.power*powerspectra_binned_A.power)) \
	#	/ np.sqrt(powereadout_noiseless_binned_B*powereadout_noiseless_binned_A)
	## NEW: Do not remove any noise.
	coherence_intrinsic_binned	= coherence_binned

	## --- Coherence Errors ---
	## Mainly from Vaughan & Nowak (1997) (Hereafter VN97) and racrs.pro
	## Assuming High Powers and High Measured Coherence

	## Use the same notation as VN97
	m	= powerspectra_binned_B.m	## Number of Segments (# Error averaging from sections and rebinning)(?)
	N_1	= white_noise_A									## White Noise (A); N^2 in VN97
	N_2	= white_noise_B									## White Noise (B); N^2 in VN97
	S_1	= np.array(powereadout_noiseless_binned_A)		## Noiseless Power Spectra (A);	S^2 in VN97
	S_2	= np.array(powereadout_noiseless_binned_B)		## Noiseless Power Spectra (B); S^2 in VN97
	P_1	= np.array(powerspectra_binned_A.power)			## Noisy Power Spectra (A)
	P_2	= np.array(powerspectra_binned_B.power)			## Noisy Power Spectra (B)
	C	= np.array(coherence_binned * (S_1*S_2))		## Complex-valued Cross Spectrum; C^2 in VN97
	n	= (S_1*N_2 + S_2*N_1 + N_1*N_2) / m				## Simplification; n^2 in VN97

	## --- New Error Calculations: (Done as close as possible to VN97 - 2021/08/04) ---
	err_1 = np.sqrt( 2 / m ) * (1-coherence_intrinsic_binned)/np.sqrt(coherence_intrinsic_binned)	## Bendat & Piersol 1986
	err_1 = (m * err_1**2 ) / coherence_intrinsic_binned**2											## Fourth term in the error equation

	err_coherence = ( (2 * n**2 * m) / ((C - n)**2) ) \
		+ (N_1/S_1)**2 + (N_2/S_2)**2 \
		+ err_1																	## Terms 1, 2+3,s 4

	err_coherence = np.sqrt(err_coherence)/np.sqrt(m)							## Deal with the m

	err_coherence_upper = ( (C - n) / (S_1 * S_2) ) * (1 + err_coherence)		## Upper bound
	err_coherence_lower = ( (C - n) / (S_1 * S_2) ) * (1 - err_coherence)		## Lower bound

	## Average the upper and lower bounds for a simple (fractional) error value
	err_coherence_intrinsic_binned_frac = err_coherence_upper
	for i in range(len(err_coherence_upper)):
		err_coherence_intrinsic_binned_frac[i] = np.mean([abs(err_coherence_upper[i] - coherence_intrinsic_binned[i]), abs(err_coherence_lower[i] - coherence_intrinsic_binned[i])])

	err_coherence_intrinsic_binned = coherence_intrinsic_binned * err_coherence_intrinsic_binned_frac

	## --- Freq Lags ---
	freq_lags, freq_lags_err = cross_spectrum_binned.time_lag()
	freq_lags_inv = -freq_lags

	## --- Phase Lags ---
	phase_lags				= []
	phase_lags_err			= []
	for j in range(0, len(freq_lags)):
		phase_lags.append(freq_lags[j] * 2 * np.pi * cross_spectrum_binned.freq[j])
		phase_lags_err.append(freq_lags_err[j] * 2 * np.pi * cross_spectrum_binned.freq[j])

	phase_lags2		= np.array(phase_lags)

	'''
	Phase lags can often go over the +/- pi boundary.
	In order to solve this problem, we assume a continuous distribution.
	First, a reference point is stated. From there, we work back and forwards.
	With each point, we look at how it relates to the mean of the previous three points.
	If it's closer to that value when adding/minusing 2pi,
		we do just that to that value and all others yet to be investigated.
	'''
	reference_point = place(reference_freq, powerspectra_binned_B.freq.real)

	## To lower frequencies...
	for j in range(reference_point-1,-1,-1):
		phase_diff = phase_lags2[j] - np.mean([phase_lags2[(j+1):(j+3)]])
		if j > 0:
			if phase_diff > np.pi:
				phase_lags2[0:j+1] -= 2 * np.pi
			if phase_diff < -np.pi:
				phase_lags2[0:j+1] += 2 * np.pi
		if j == 0:
			if phase_diff > np.pi:
				phase_lags2[0] -= 2 * np.pi
			if phase_diff < -np.pi:
				phase_lags2[0] += 2 * np.pi

	## To higher frequencies...
	for j in range(reference_point+1,len(phase_lags2),1):
		phase_diff = phase_lags2[j] - np.mean([phase_lags2[(j-2):(j)]])
		if phase_diff > np.pi:
			phase_lags2[j:len(phase_lags2)] -= 2 * np.pi
		if phase_diff < -np.pi:
			phase_lags2[j:len(phase_lags2)] += 2 * np.pi

	freq_lags2 = []
	freq_lags2_inv = []
	for j in range(0, len(phase_lags2)):
		freq_lags2.append(phase_lags2[j] / ( 2 * np.pi * cross_spectrum_binned.freq[j]))
		freq_lags2_inv.append(-freq_lags2[j])

	freq_lags_err[0] = segment_size

	## Cut out the last bin; in testing, this always seems to be based on a single point after logarithmic binning.
	coherence_freqs			= powerspectra_binned_B.freq.real[0:-1]
	powerspectra_A			= powereadout_noiseless_binned_A[0:-1]
	powerspectra_A_err		= powerspectra_binned_A.power_err.real[0:-1]
	powerspectra_B			= powereadout_noiseless_binned_B[0:-1]
	powerspectra_B_err		= powerspectra_binned_B.power_err.real[0:-1]
	coherence				= coherence_intrinsic_binned[0:-1]
	coherence_err			= err_coherence_intrinsic_binned[0:-1]
	phase_lags				= phase_lags[0:-1]
	phase_lags_err			= phase_lags_err[0:-1]
	freq_lags				= freq_lags2[0:-1]
	freq_lags_inv			= freq_lags2_inv[0:-1]
	freq_lags_err			= freq_lags_err[0:-1]

	return [coherence_freqs, powerspectra_A, powerspectra_A_err, powerspectra_B, powerspectra_B_err,
		coherence, coherence_err, phase_lags, phase_lags_err, freq_lags, freq_lags_inv, freq_lags_err]
